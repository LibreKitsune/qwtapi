// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "indicatorsjsonconverter.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

IndicatorsJsonConverter::IndicatorsJsonConverter(QObject *parent)
    : QObject{parent}
{

}

Indicators* IndicatorsJsonConverter::indicatorsFromJson(QString json)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toLocal8Bit());

    if (!jsonDocument.isEmpty())
    {
        if (jsonDocument.isObject())
        {
            QJsonObject jsonObject = jsonDocument.object();
            // A HudMsg object can only be created if the json object is not empty
            if (!jsonObject.isEmpty())
            {
                Indicators* indicators = new Indicators(nullptr);

                QJsonValue validValue = jsonObject.value("valid");
                indicators->setValid(validValue.toBool(false));

                QJsonValue typeValue = jsonObject.value("type");
                indicators->setType(typeValue.toString(""));

                QStringList keys = jsonObject.keys();
                foreach(QString key, keys)
                {
                    indicators->addIndicator(key,jsonObject.value(key).toVariant());
                }

                return indicators;
            }

        }
    }

    return nullptr;
}

QJsonObject IndicatorsJsonConverter::toJson(Indicators* indicators)
{
    QJsonObject json;

    json["valid"] = indicators->valid();
    json["type"] = indicators->type();

    foreach(QString key, indicators->indicators().keys())
    {
        QVariant value = indicators->indicators().value(key);
        json[key] = QJsonValue::fromVariant(value);
    }

    return json;
}
