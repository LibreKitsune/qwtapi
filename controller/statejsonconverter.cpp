// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "statejsonconverter.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

StateJsonConverter::StateJsonConverter(QObject *parent)
    : QObject{parent}
{

}

State* StateJsonConverter::stateFromJson(QString json)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toLocal8Bit());

    if (!jsonDocument.isEmpty())
    {
        if (jsonDocument.isObject())
        {
            QJsonObject jsonObject = jsonDocument.object();
            // A HudMsg object can only be created if the json object is not empty
            if (!jsonObject.isEmpty())
            {
                State* state = new State(nullptr);

                addState(state, jsonObject);
                addEngines(state, jsonObject);

                return state;
            }
        }
    }

    return nullptr;
}

QJsonObject StateJsonConverter::toJson(State* state)
{
   QJsonObject json;

   json["valid"] = state->valid();
   json["aileron, %"] = state->aileronPercent();
   json["elevator, %"] = state->elevatorPercent();
   json["rudder, %"] = state->rudderPercent();
   json["flaps, %"] = state->flapsPercent();
   json["gear, %"] = state->gearPercent();
   json["airbrake, %"] = state->airbrakePercent();
   json["H, m"] = state->altitude();
   json["TAS, km/h"] = state->trueAirSpeed();
   json["IAS, km/h"] = state->indicatedAirSpeed();
   json["M"] = state->mach();
   json["AoA, deg"] = state->angleOfAttack();
   json["AoS, deg"] = state->angleOfSideslip();
   json["Ny"] = state->gForce();
   json["Vy, m/s"] = state->climbRate();
   json["Wx, deg/s"] = state->rollRate();
   json["Mfuel, kg"] = state->currentFuel();
   json["Mfuel0, kg"] = state->maximumFuel();
   json["FuelConsumption"] = state->fuelConsumption();
   json["FuelTimeRemaining"] = state->fuelTimeRemaining();

   int i = 1;
   foreach(EngineState* engine, state->engineStates())
   {
       QString engineNumber = QString::number(i);

       json[QString("throttle %1, %").arg(engineNumber)] = engine->throttle();
       json[QString("RPM throttle %1, %").arg(engineNumber)] = engine->rpmThrottle();
       json[QString("radiator %1, %").arg(engineNumber)] = engine->radiator();
       json[QString("magneto %1").arg(engineNumber)] = engine->magneto();
       json[QString("power %1, hp").arg(engineNumber)] = engine->power();
       json[QString("RPM %1").arg(engineNumber)] = engine->rpm();
       json[QString("manifold pressure %1, atm").arg(engineNumber)] = engine->manifoldPressure();
       json[QString("water temp %1, C").arg(engineNumber)] = engine->waterTemp();
       json[QString("oil temp %1, C").arg(engineNumber)] = engine->oilTemp();
       json[QString("pitch %1, deg").arg(engineNumber)] = engine->pitch();
       json[QString("thrust %1, kgs").arg(engineNumber)] = engine->thrust();
       json[QString("efficiency %1, %").arg(engineNumber)] = engine->efficiency();

       i++;
   }

   return json;
}

void StateJsonConverter::addState(State *state, QJsonObject json)
{
    QJsonValue validValue = json.value("valid");
    state->setValid(validValue.toBool(false));

    QJsonValue aileronValue = json.value("aileron, %");
    state->setAileronPercent(aileronValue.toInt(0));

    QJsonValue elevatorValue = json.value("elevator, %");
    state->setElevatorPercent(elevatorValue.toInt(0));

    QJsonValue rudderValue = json.value("rudder, %");
    state->setRudderPercent(rudderValue.toInt(0));

    QJsonValue flapsValue = json.value("flaps, %");
    state->setFlapsPercent(flapsValue.toInt(0));

    QJsonValue gearValue = json.value("gear, %");
    state->setGearPercent(gearValue.toInt(0));

    QJsonValue airbrakeValue = json.value("airbrake, %");
    state->setAirbrakePercent(airbrakeValue.toInt(0));

    QJsonValue altitudeValue = json.value("H, m");
    state->setAltitude(altitudeValue.toInt(0));

    QJsonValue tasValue = json.value("TAS, km/h");
    state->setTrueAirSpeed(tasValue.toInt(0));

    QJsonValue iasValue = json.value("IAS, km/h");
    state->setIndicatedAirSpeed(iasValue.toInt(0));

    QJsonValue machValue = json.value("M");
    state->setMach(machValue.toDouble(0.0));

    QJsonValue aoaValue = json.value("AoA, deg");
    state->setAngleOfAttack(aoaValue.toDouble(0.0));

    QJsonValue aosValue = json.value("AoS, deg");
    state->setAngleOfSideslip(aosValue.toDouble(0.0));

    QJsonValue nyValue = json.value("Ny");
    state->setGForce(nyValue.toDouble(0.0));

    QJsonValue vyValue = json.value("Vy, m/s");
    state->setClimbRate(vyValue.toDouble(0.0));

    QJsonValue wxValue = json.value("Wx, deg/s");
    state->setRollRate(wxValue.toInt(0));

    QJsonValue fuelValue = json.value("Mfuel, kg");
    state->setCurrentFuel(fuelValue.toInt(0));

    QJsonValue fuel0Value = json.value("Mfuel0, kg");
    state->setMaximumFuel(fuel0Value.toInt(0));

    QJsonValue fuelConsumptionValue = json.value("FuelConsumption");
    state->setFuelConsumption(fuelConsumptionValue.toDouble(0));

    QJsonValue fuelTimeRemainingValue = json.value("FuelTimeRemaining");
    state->setFuelTimeRemaining(fuelTimeRemainingValue.toDouble(0));
}

void StateJsonConverter::addEngines(State *state, QJsonObject json)
{
    int currentEngineNumber = 1;
    bool nextHigherExists = false;
    do {

        addEngine(state, json, currentEngineNumber);

        currentEngineNumber++;
        nextHigherExists = json.contains(QString("RPM %1").arg(currentEngineNumber));

    } while (nextHigherExists);
    state->setEngineCount(currentEngineNumber - 1);
}

void StateJsonConverter::addEngine(State *state, QJsonObject json, int engineNumber)
{
    EngineState* engine = new EngineState(nullptr);

    QJsonValue throttleValue = json.value(QString("throttle %1, %").arg(engineNumber));
    engine->setThrottle(throttleValue.toInt(0));

    QJsonValue rpmThrottleValue = json.value(QString("RPM throttle %1, %").arg(engineNumber));
    engine->setRpmThrottle(rpmThrottleValue.toInt(0));

    QJsonValue radiatorValue = json.value(QString("radiator %1, %").arg(engineNumber));
    engine->setRadiator(radiatorValue.toInt(0));

    QJsonValue magnetoValue = json.value(QString("magneto %1").arg(engineNumber));
    engine->setMagneto(magnetoValue.toInt(0));

    QJsonValue powerValue = json.value(QString("power %1, hp").arg(engineNumber));
    engine->setPower(powerValue.toDouble(0.0));

    QJsonValue rpmValue = json.value(QString("RPM %1").arg(engineNumber));
    engine->setRpm(rpmValue.toInt(0));

    QJsonValue manifoldValue = json.value(QString("manifold pressure %1, atm").arg(engineNumber));
    engine->setManifoldPressure(manifoldValue.toDouble(0.0));

    QJsonValue waterTempValue = json.value(QString("water temp %1, C").arg(engineNumber));
    engine->setWaterTemp(waterTempValue.toInt(0));

    QJsonValue oilTempValue = json.value(QString("oil temp %1, C").arg(engineNumber));
    engine->setOilTemp(oilTempValue.toInt(0));

    QJsonValue pitchValue = json.value(QString("pitch %1, deg").arg(engineNumber));
    engine->setPitch(pitchValue.toDouble(0.0));

    QJsonValue thrustValue = json.value(QString("thrust %1, kgs").arg(engineNumber));
    engine->setThrust(thrustValue.toInt(0));

    QJsonValue efficiencyValue = json.value(QString("efficiency %1, %").arg(engineNumber));
    engine->setEfficiency(efficiencyValue.toInt(0));

    state->addEngineState(engine);
}
