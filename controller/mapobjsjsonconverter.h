// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPOBJSJSONCONVERTER_H
#define MAPOBJSJSONCONVERTER_H

#include <QObject>
#include <QMap>
#include "qwtapi/model/mapobj.h"
#include "qwtapi/model/mapobjs.h"

class MapObjsJsonConverter : public QObject
{
    Q_OBJECT
public:
    explicit MapObjsJsonConverter(QObject *parent = nullptr);
    MapObjs* mapObjsFromJson(QString json);
    QJsonArray toJson(MapObjs* mapObjs);

private:
    void addMapObj(MapObjs* mapObjs, QJsonObject json);
    MapObj::Type jsonValueToType(QJsonValue jsonValue);
    MapObj::Icon jsonValueToIcon(QJsonValue jsonValue);
    QColor jsonValueToColour(QJsonValue jsonValue);
    QPointF jsonValuesToPoint(QJsonValue xValue, QJsonValue yValue);

    QMap<QString, MapObj::Type> m_typemap;
    QMap<QString, MapObj::Icon> m_iconmap;


signals:

};

#endif // MAPOBJSJSONCONVERTER_H
