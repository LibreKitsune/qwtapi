// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapobjsjsonconverter.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

MapObjsJsonConverter::MapObjsJsonConverter(QObject *parent)
    : QObject{parent}
{
    m_typemap.insert("aircraft", MapObj::Type::AIRCRAFT);
    m_typemap.insert("airfield", MapObj::Type::AIRFIELD);
    m_typemap.insert("bombing_point", MapObj::Type::BOMBING_POINT);
    m_typemap.insert("defending_point", MapObj::Type::DEFENDING_POINT);
    m_typemap.insert("ground_model", MapObj::Type::GROUND_MODEL);
    m_typemap.insert("respawn_base_bomber", MapObj::Type::RESPAWN_BASE_BOMBER);
    m_typemap.insert("unknown", MapObj::Type::UNKNOWN);

    m_iconmap.insert("Airdefence", MapObj::Icon::AIRDEFENCE);
    m_iconmap.insert("Fighter", MapObj::Icon::FIGHTER);
    m_iconmap.insert("Player", MapObj::Icon::PLAYER);
    m_iconmap.insert("SPAA", MapObj::Icon::SPAA);
    m_iconmap.insert("Tracked", MapObj::Icon::TRACKED);
    m_iconmap.insert("Wheeled", MapObj::Icon::WHEELED);
    m_iconmap.insert("bombing_point", MapObj::Icon::BOMBING_POINT);
    m_iconmap.insert("defending_point", MapObj::Icon::DEFENDING_POINT);
    m_iconmap.insert("none", MapObj::Icon::NONE);
    m_iconmap.insert("respawn_base_bomber", MapObj::Icon::RESPAWN_BASE_BOMBER);
    m_iconmap.insert("unknown", MapObj::Icon::UNKNOWN);

}

MapObjs* MapObjsJsonConverter::mapObjsFromJson(QString json)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toLocal8Bit());

    if (!jsonDocument.isEmpty())
    {
        if (jsonDocument.isArray())
        {
            QJsonArray jsonArray = jsonDocument.array();

            MapObjs* mapObjs = new MapObjs(nullptr);

            foreach(QJsonValue jsonValue, jsonArray)
            {
                if (jsonValue.isObject())
                {
                    addMapObj(mapObjs, jsonValue.toObject());
                }
            }

            return mapObjs;

        }
    }

    return nullptr;
}

QJsonArray MapObjsJsonConverter::toJson(MapObjs* mapObjs)
{
    QJsonArray json;

    foreach(MapObj* obj, mapObjs->objs())
    {
        QJsonObject jsonObj;

        jsonObj["type"] = m_typemap.key(obj->type(),"unknown");
        jsonObj["color"] = obj->colour().name(QColor::HexRgb);
        jsonObj["blink"] = obj->blink() ? 1 : 0;
        jsonObj["icon"] = m_iconmap.key(obj->icon(),"unknown");
        jsonObj["icon_bg"] = m_iconmap.key(obj->iconBG(),"unknown");
        jsonObj["sx"] = obj->start().x();
        jsonObj["sy"] = obj->start().y();
        jsonObj["ex"] = obj->end().x();
        jsonObj["ey"] = obj->end().y();
        jsonObj["dx"] = obj->direction().x();
        jsonObj["dy"] = obj->direction().y();
        jsonObj["x"] = obj->coordinates().x();
        jsonObj["y"] = obj->coordinates().y();

        json.append(jsonObj);
    }

    return json;
}

void MapObjsJsonConverter::addMapObj(MapObjs *mapObjs, QJsonObject json)
{
    if (!json.isEmpty())
    {
        MapObj* obj = new MapObj(nullptr);

        QJsonValue typeValue = json.value("type");
        obj->setType(jsonValueToType(typeValue));

        QJsonValue colourValue = json.value("color");
        obj->setColour(jsonValueToColour(colourValue));

        QJsonValue blinkValue = json.value("blink");
        obj->setBlink(blinkValue.toInt(0) == 1);

        QJsonValue iconValue = json.value("icon");
        obj->setIcon(jsonValueToIcon(iconValue));

        QJsonValue iconBGValue = json.value("icon_bg");
        obj->setIconBG(jsonValueToIcon(iconBGValue));

        QJsonValue sxValue = json.value("sx");
        QJsonValue syValue = json.value("sy");
        obj->setStart(jsonValuesToPoint(sxValue, syValue));

        QJsonValue exValue = json.value("ex");
        QJsonValue eyValue = json.value("ey");
        obj->setEnd(jsonValuesToPoint(exValue, eyValue));

        QJsonValue dxValue = json.value("dx");
        QJsonValue dyValue = json.value("dy");
        obj->setDirection(jsonValuesToPoint(dxValue, dyValue));

        QJsonValue xValue = json.value("x");
        QJsonValue yValue = json.value("y");
        obj->setCoordinates(jsonValuesToPoint(xValue, yValue));

        mapObjs->addObj(obj);
    }
}

MapObj::Type MapObjsJsonConverter::jsonValueToType(QJsonValue jsonValue)
{
    QString value = jsonValue.toString("unknown");

    return m_typemap.value(value,MapObj::Type::UNKNOWN);
}

MapObj::Icon MapObjsJsonConverter::jsonValueToIcon(QJsonValue jsonValue)
{
    QString value = jsonValue.toString("unknown");

    return m_iconmap.value(value,MapObj::Icon::UNKNOWN);
}

QColor MapObjsJsonConverter::jsonValueToColour(QJsonValue jsonValue)
{
    QString value = jsonValue.toString("#000000");
    return QColor(value);
}

QPointF MapObjsJsonConverter::jsonValuesToPoint(QJsonValue xValue, QJsonValue yValue)
{
    if (xValue.isDouble() && yValue.isDouble())
    {
        double x = xValue.toDouble(0);
        double y = yValue.toDouble(0);
        return QPointF(x,y);
    }

    return QPointF(0,0);
}
