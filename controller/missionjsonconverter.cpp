// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "missionjsonconverter.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

MissionJsonConverter::MissionJsonConverter(QObject *parent)
    : QObject{parent}
{

    m_statusmap.insert("in_progress",Objective::Status::IN_PROGRESS);
    m_statusmap.insert("undefined",Objective::Status::UNDEFINED);
    m_statusmap.insert("running",Objective::Status::RUNNING);
    m_statusmap.insert("success",Objective::Status::SUCCESS);
    m_statusmap.insert("fail",Objective::Status::FAIL);
    m_statusmap.insert("unknown",Objective::Status::UNKNOWN);

}

 Mission* MissionJsonConverter::missionFromJson(QString json)
{
     QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toLocal8Bit());

     if (!jsonDocument.isEmpty())
     {
         if (jsonDocument.isObject())
         {
             QJsonObject jsonObject = jsonDocument.object();

             if (!jsonObject.isEmpty())
             {
                 Mission* mission = new Mission(nullptr);

                 QJsonValue statusValue = jsonObject.value("status");
                 mission->setStatus(jsonValueToStatus(statusValue));

                 QJsonValue objectivesValue = jsonObject.value("objectives");
                 if (objectivesValue.isArray())
                 {
                     foreach(QJsonValue value, objectivesValue.toArray())
                     {
                         if (value.isObject())
                         {
                             addObjective(mission, value.toObject());
                         }
                     }
                 }

                 return mission;
             }
         }
     }

     return nullptr;
}

 QJsonObject MissionJsonConverter::toJson(Mission* mission)
 {
    QJsonObject json;

    json["status"] = statusToString(mission->status());

    QJsonArray objectives;
    foreach (Objective* objective, mission->objectives())
    {
        QJsonObject objJson;
        objJson["primary"] = objective->primary();
        objJson["status"] = statusToString(objective->status());
        objJson["text"] = objective->text();
        objectives.append(objJson);
    }

    json["objectives"] = objectives;

    return json;
 }

 void MissionJsonConverter::addObjective(Mission *mission, QJsonObject json)
 {
    if (!json.isEmpty())
    {
        Objective* objective = new Objective(nullptr);

        QJsonValue primaryValue = json.value("primary");
        objective->setPrimary(primaryValue.toBool(false));

        QJsonValue statusValue = json.value("status");
        objective->setStatus(jsonValueToStatus(statusValue));

        QJsonValue textValue = json.value("text");
        // Replace <color> tags with <font color=> tags to correctly show colour
        // Replace @team###Color with actuall colour values
        // Replace some special chars with point names
        objective->setText(textValue.toString("")
                           .replace("<color","<font color")
                           .replace("</color>","</font>")
                           .replace("@teamRedColor","#FF0000")
                           .replace("@teamBlueColor","#0000FF")
                           .replace("╸","(A)")
                           .replace("╹","(B)")
                           .replace("╺","(C)")
                           .replace("╶","⯁"));

        mission->addObjective(objective);
    }
 }

 Objective::Status MissionJsonConverter::jsonValueToStatus(QJsonValue jsonValue)
 {
     QString value = jsonValue.toString("unknown");

     return m_statusmap.value(value,Objective::Status::UNKNOWN);
 }

 QString MissionJsonConverter::statusToString(Objective::Status status)
 {
    return m_statusmap.key(status,"unknown");
 }
