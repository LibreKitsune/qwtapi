// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "hudmsgjsonconverter.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

HudMsgJsonConverter::HudMsgJsonConverter(QObject *parent)
    : QObject{parent}
{

}

HudMsg* HudMsgJsonConverter::hudMsgFromJson(QString json)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toLocal8Bit());

    if (!jsonDocument.isEmpty())
    {
        if (jsonDocument.isObject())
        {
            QJsonObject jsonObject = jsonDocument.object();
            // A HudMsg object can only be created if the json object is not empty
            if (!jsonObject.isEmpty())
            {
                HudMsg* msg = new HudMsg(nullptr);
                addEvents(msg, jsonObject);
                addDamage(msg, jsonObject);
                return msg;
            }
        }
    }

    return nullptr;
}

QJsonObject HudMsgJsonConverter::toJson(HudMsg* msg)
{
    QJsonObject json;

    QJsonArray eventArray;
    QJsonArray damageArray;

    foreach (HudMsgEvent* event, msg->events())
    {
        QJsonObject eventObj;
        eventObj["id"] = event->id();
        eventObj["msg"] = event->msg();
        eventObj["sender"] = event->sender();
        eventObj["enemy"] = event->enemy();
        eventObj["mode"] = event->mode();
        eventObj["time"] = event->time();
        eventArray.append(eventObj);
    }

    foreach (HudMsgDamage* damage, msg->damageEvents())
    {
        QJsonObject damageObj;
        damageObj["id"] = damage->id();
        damageObj["msg"] = damage->msg();
        damageObj["sender"] = damage->sender();
        damageObj["enemy"] = damage->enemy();
        damageObj["mode"] = damage->mode();
        damageObj["time"] = damage->time();
        damageArray.append(damageObj);
    }

    json["events"] = eventArray;
    json["damage"] = damageArray;

    return json;
}

void HudMsgJsonConverter::addEvents(HudMsg *msg, QJsonObject jsonObject)
{
    QJsonValue eventObjects = jsonObject.value("events");

    if (eventObjects.isArray())
    {
        QJsonArray events = eventObjects.toArray();
        if (!events.isEmpty())
        {
            QList<HudMsgEvent*> eventList;
            for (int i = 0; i < events.count(); i++)
            {
                QJsonValue eventElement = events.at(i);
                if (eventElement.isObject())
                {
                    HudMsgEvent* event = eventFromJson(eventElement);
                    if (event != nullptr) {
                        event->setParent(msg);
                        eventList.append(event);
                    }
                }
            }

            std::sort(eventList.begin(), eventList.end(), [](HudMsgEvent* a, HudMsgEvent* b) {
                return a->id() < b->id();
            });

            msg->setEvents(eventList);
        }
    }
}

void HudMsgJsonConverter::addDamage(HudMsg *msg, QJsonObject jsonObject)
{
    QJsonValue damageObjects = jsonObject.value("damage");


    if (damageObjects.isArray())
    {
        QJsonArray damageEvents = damageObjects.toArray();
        if (!damageEvents.isEmpty())
        {
            QList<HudMsgDamage*> damageList;
            for (int i = 0; i < damageEvents.count(); i++)
            {
                QJsonValue damageElement = damageEvents.at(i);
                if (damageElement.isObject())
                {
                    HudMsgDamage* damageEvent = damageFromJson(damageElement);
                    if (damageEvent != nullptr) {
                        damageEvent->setParent(msg);
                        damageList.append(damageEvent);
                    }
                }
            }
            std::sort(damageList.begin(), damageList.end(), [](HudMsgDamage* a, HudMsgDamage* b) {
                return a->id() < b->id();
            });

            msg->setDamageEvents(damageList);
        }
    }
}

HudMsgEvent* HudMsgJsonConverter::eventFromJson(QJsonValue jsonValue)
{
    HudMsgEvent* event = nullptr;

    if (jsonValue.isObject()) {
        QJsonObject object = jsonValue.toObject();

        if (!object.isEmpty()) {
             event = new HudMsgEvent(nullptr);

             QJsonValue idValue = object.value("id");
             event->setId(idValue.toInt(0));

             QJsonValue msgValue = object.value("msg");
             event->setMsg(msgValue.toString(""));

             QJsonValue senderValue = object.value("sender");
             event->setSender(senderValue.toString(""));

             QJsonValue enemyValue = object.value("enemy");
             event->setEnemy(enemyValue.toBool(false));

             QJsonValue modeValue = object.value("mode");
             event->setMode(enemyValue.toString(""));

             QJsonValue timeValue = object.value("time");
             event->setTime(timeValue.toInt(0));
        }
    }

    return event;
}

HudMsgDamage* HudMsgJsonConverter::damageFromJson(QJsonValue jsonValue)
{
    HudMsgDamage* damage = nullptr;

    if (jsonValue.isObject()) {
        QJsonObject object = jsonValue.toObject();

        if (!object.isEmpty()) {
             damage = new HudMsgDamage(nullptr);

             QJsonValue idValue = object.value("id");
             damage->setId(idValue.toInt(0));

             QJsonValue msgValue = object.value("msg");
             damage->setMsg(msgValue.toString(""));

             QJsonValue senderValue = object.value("sender");
             damage->setSender(senderValue.toString(""));

             QJsonValue enemyValue = object.value("enemy");
             damage->setEnemy(enemyValue.toBool(false));

             QJsonValue modeValue = object.value("mode");
             damage->setMode(enemyValue.toString(""));

             QJsonValue timeValue = object.value("time");
             damage->setTime(timeValue.toInt(0));

             parseMsg(damage);
        }
    }

    return damage;
}

void HudMsgJsonConverter::parseMsg(HudMsgDamage *damage) {
    QString msg = damage->msg();

    Subject* subject = nullptr;
    Subject* target = nullptr;

    // Parse event messages
    if (msg.contains("shot down"))
    {
        damage->setType(HudMsgDamage::Type::SHOT_DOWN);
        int idx = msg.indexOf("shot down");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

        // Right of keyword
        idx = idx + 9;
        target = parseSubject( msg.mid(idx));

    }
    else if (msg.contains("damaged"))
    {
        damage->setType(HudMsgDamage::Type::DAMAGED);
        int idx = msg.indexOf("damaged");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

        // Right of keyword
        idx = idx + 7;
        target = parseSubject( msg.mid(idx));

    }
    else if (msg.contains("set afire"))
    {
        damage->setType(HudMsgDamage::Type::SET_AFIRE);
        int idx = msg.indexOf("set afire");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

        // Right of keyword
        idx = idx + 9;
        target = parseSubject( msg.mid(idx));

    }
    else if (msg.contains("has achieved"))
    {
        damage->setType(HudMsgDamage::Type::ACHIEVED);
        int idx = msg.indexOf("has achieved");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

        // Right of keyword
        idx = idx + 12;
        // Achievements require special parsing
        target = parseAchievement( msg.mid(idx));

    }
    else if (msg.endsWith("has disconnected from the game.") || msg.endsWith("td! kd?NET_PLAYER_DISCONNECT_FROM_GAME"))
    {
        damage->setType(HudMsgDamage::Type::DISCONNECTED);
        // Removing keywords is enough
        QString sub = msg;
        sub = sub.replace("has disconnected from the game.","");
        sub = sub.replace("td! kd?NET_PLAYER_DISCONNECT_FROM_GAME","");

        subject = parseSubject(sub);

    }
    else if (msg.contains("destroyed"))
    {
        damage->setType(HudMsgDamage::Type::DESTROYED);
        int idx = msg.indexOf("destroyed");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

        // Right of keyword
        idx = idx + 9;
        target = parseSubject( msg.mid(idx));

    }
    else if (msg.endsWith("has crashed."))
    {
        damage->setType(HudMsgDamage::Type::CRASHED);
        int idx = msg.indexOf("has crashed.");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

    }
    else if (msg.endsWith("has delivered the first strike!"))
    {
        damage->setType(HudMsgDamage::Type::FIRST_STRIKE);
        int idx = msg.indexOf("has delivered the first strike!");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

    }
    else if (msg.endsWith("has delivered the final blow!"))
    {
        damage->setType(HudMsgDamage::Type::FINAL_BLOW);
        int idx = msg.indexOf("has delivered the final blow!");

        // Left of keyword
        QStringRef subjectRef(&msg, 0,idx);
        subject = parseSubject(subjectRef.toString());

    }
    else
    {
        damage->setType(HudMsgDamage::Type::UNKNOWN);
    }

    // Default empty subject
    if (subject == nullptr)
    {
        subject = new Subject();
    }

    // Default empty target
    if (target == nullptr)
    {
        target = new Subject();
    }

    damage->setSubject(subject);
    damage->setTarget(target);

}

Subject* HudMsgJsonConverter::parseSubject(QString subject)
{
   Subject* subjectPtr = new Subject();
   QString trimmedSubject = subject.trimmed();

   QString vehicle = "";


   // Vehicle parsing needs to happen before split since the vehicle name can contain spaces
   if (trimmedSubject.contains("(")) {
       QString raw = trimmedSubject;
       int start = raw.indexOf("(");
       int end = raw.indexOf(")");
       int length = (end - start)+1;

       // Get the vehicle part via a StringRef
       QStringRef vehicleRef(&raw, start, length);
       vehicle = vehicleRef.toString();
       // Remove the vehicle from the subject
       trimmedSubject = trimmedSubject.replace(vehicle, "").trimmed();

       // Remove the braces
       vehicle = vehicle.replace("(","").replace(")","");
   }

   QStringList components = trimmedSubject.split(" ");

   switch (components.size()) {
       case 1:
           subjectPtr->setName(components.at(0));
           break;
       case 2:

           subjectPtr->setClan(components.at(0));
           subjectPtr->setName(components.at(1));
           break;
       default:
           subjectPtr->setName("");
           subjectPtr->setClan("");
   }
   subjectPtr->setVehicle(vehicle);
   return subjectPtr;
}

Subject* HudMsgJsonConverter::parseAchievement(QString achievement)
{
   Subject* subjectPtr = new Subject();
   QString substr = achievement.trimmed();
   substr = substr.replace("\"","");
   subjectPtr->setName(substr);
   return subjectPtr;

}

