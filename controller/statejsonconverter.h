// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef STATEJSONCONVERTER_H
#define STATEJSONCONVERTER_H

#include <QObject>
#include "qwtapi/model/state.h"

class StateJsonConverter : public QObject
{
    Q_OBJECT
public:
    explicit StateJsonConverter(QObject *parent = nullptr);
    State* stateFromJson(QString json);
    QJsonObject toJson(State* state);

private:
    void addState(State* state, QJsonObject json);
    void addEngines(State* state, QJsonObject json);
    void addEngine(State* state, QJsonObject json, int engineNumber);

signals:

};

#endif // STATEJSONCONVERTER_H
