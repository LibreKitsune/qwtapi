// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "chatmessagejsonconverter.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

ChatMessageJsonConverter::ChatMessageJsonConverter(QObject *parent)
    : QObject{parent}
{
    m_modemap.insert("Team", ChatMessage::Mode::TEAM);
    m_modemap.insert("Squad", ChatMessage::Mode::SQUAD);
    m_modemap.insert("All", ChatMessage::Mode::ALL);
    m_modemap.insert("System", ChatMessage::Mode::SYSTEM);
    m_modemap.insert("Unknown", ChatMessage::Mode::UNKNOWN);

}

ChatMessages* ChatMessageJsonConverter::chatMessagesFromJson(QString json)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toLocal8Bit());

    if (!jsonDocument.isEmpty())
    {
        if (jsonDocument.isArray())
        {
            QJsonArray jsonArray = jsonDocument.array();

            ChatMessages* chatMessages = new ChatMessages(nullptr);

            foreach(QJsonValue value, jsonArray)
            {
                if (value.isObject())
                {
                    addChatMessage(chatMessages, value.toObject());
                }
            }

            return chatMessages;

        }
    }

    return nullptr;
}

QJsonArray ChatMessageJsonConverter::toJson(ChatMessages* messages)
{
    QJsonArray json;

    foreach(ChatMessage* message, messages->messages())
    {
        QJsonObject messageObj;
        messageObj["id"] = message->id();
        messageObj["msg"] = message->msg();
        messageObj["enemy"] = message->enemy();
        messageObj["mode"] = m_modemap.key(message->mode(),"Unknown");
        messageObj["time"] = message->time();
        messageObj["sender"] = message->sender();
        json.append(messageObj);
    }

    return json;
}

void ChatMessageJsonConverter::addChatMessage(ChatMessages *messages, QJsonObject json)
{
    if (!json.isEmpty())
    {
        ChatMessage* message = new ChatMessage();

        QJsonValue idValue = json.value("id");
        message->setId(idValue.toInt(0));

        QJsonValue msgValue = json.value("msg");
        message->setMsg(msgValue.toString("").replace("<color","<font color").replace("</color>","</font>"));

        QJsonValue enemyValue = json.value("enemy");
        message->setEnemy(enemyValue.toBool(false));

        QJsonValue modeValue = json.value("mode");
        message->setMode(jsonValueToMode(modeValue));

        QJsonValue timeValue = json.value("time");
        message->setTime(timeValue.toInt(0));

        QJsonValue senderValue = json.value("sender");
        message->setSender(senderValue.toString(""));

        messages->addMessage(message);
    }
}

ChatMessage::Mode ChatMessageJsonConverter::jsonValueToMode(QJsonValue jsonValue)
{
    QString value = jsonValue.toString("Unknown");

    return m_modemap.value(value,ChatMessage::Mode::UNKNOWN);
}
