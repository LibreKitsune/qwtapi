// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MISSIONJSONCONVERTER_H
#define MISSIONJSONCONVERTER_H

#include <QObject>
#include <QMap>
#include "qwtapi/model/mission.h"
#include "qwtapi/model/objective.h"
class MissionJsonConverter : public QObject
{
    Q_OBJECT
public:
    explicit MissionJsonConverter(QObject *parent = nullptr);
    Mission* missionFromJson(QString json);
    QJsonObject toJson(Mission* state);

private:
    void addObjective(Mission* mission, QJsonObject json);
    Objective::Status jsonValueToStatus(QJsonValue jsonValue);
    QString statusToString(Objective::Status status);

    QMap<QString, Objective::Status> m_statusmap;
signals:

};

#endif // MISSIONJSONCONVERTER_H
