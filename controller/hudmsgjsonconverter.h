// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef HUDMSGJSONCONVERTER_H
#define HUDMSGJSONCONVERTER_H

#include <QObject>
#include "qwtapi/model/hudmsg.h"
#include "qwtapi/model/hudmsgdamage.h"
#include "qwtapi/model/hudmsgevent.h"


class HudMsgJsonConverter : public QObject
{
    Q_OBJECT


public:
    explicit HudMsgJsonConverter(QObject *parent = nullptr);
    HudMsg* hudMsgFromJson(QString json);
    QJsonObject toJson(HudMsg* msg);

private:
    HudMsgEvent* eventFromJson(QJsonValue jsonValue);
    HudMsgDamage* damageFromJson(QJsonValue jsonValue);

    void addEvents(HudMsg* msg, QJsonObject jsonObject);
    void addDamage(HudMsg* msg, QJsonObject jsonObject);

    void parseMsg(HudMsgDamage* damage);
    Subject* parseSubject(QString subject);
    Subject* parseAchievement(QString achievement);

signals:

};

#endif // HUDMSGJSONCONVERTER_H
