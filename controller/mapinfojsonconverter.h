// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPINFOJSONCONVERTER_H
#define MAPINFOJSONCONVERTER_H

#include <QObject>
#include <QJsonValue>
#include "qwtapi/model/mapinfo.h"

class MapinfoJsonConverter : public QObject
{
    Q_OBJECT
public:
    explicit MapinfoJsonConverter(QObject *parent = nullptr);
    Mapinfo* mapinfoFromJson(QString json);
    QJsonObject toJson(Mapinfo* mapInfo);

private:
    QPointF arrayToQPoint(QJsonValue array);
    QJsonArray pointToArray(QPointF point);

signals:

};

#endif // MAPINFOJSONCONVERTER_H
