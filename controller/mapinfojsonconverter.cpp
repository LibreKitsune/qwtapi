// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapinfojsonconverter.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

MapinfoJsonConverter::MapinfoJsonConverter(QObject *parent)
    : QObject{parent}
{

}

Mapinfo* MapinfoJsonConverter::mapinfoFromJson(QString json)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toLocal8Bit());

    if (!jsonDocument.isEmpty())
    {
        if (jsonDocument.isObject())
        {
            QJsonObject jsonObject = jsonDocument.object();
            // A HudMsg object can only be created if the json object is not empty
            if (!jsonObject.isEmpty())
            {
                Mapinfo* mapInfo = new Mapinfo(nullptr);

                QJsonValue gridSizeValue = jsonObject.value("grid_size");
                mapInfo->setGridSize(arrayToQPoint(gridSizeValue));

                QJsonValue gridStepsValue = jsonObject.value("grid_steps");
                mapInfo->setGridSteps(arrayToQPoint(gridStepsValue));

                QJsonValue gridZeroValue = jsonObject.value("grid_zero");
                mapInfo->setGridZero(arrayToQPoint(gridZeroValue));

                QJsonValue mapGenerationValue = jsonObject.value("map_generation");
                mapInfo->setMapGeneration(mapGenerationValue.toInt(0));

                QJsonValue mapMaxValue = jsonObject.value("map_max");
                mapInfo->setMapMax(arrayToQPoint(mapMaxValue));

                QJsonValue mapMinValue = jsonObject.value("map_min");
                mapInfo->setMapMin(arrayToQPoint(mapMinValue));

                return mapInfo;
            }
        }
    }

    return nullptr;
}

QJsonObject MapinfoJsonConverter::toJson(Mapinfo* mapInfo)
{
    QJsonObject json;

    json["grid_size"] = pointToArray(mapInfo->gridSize());
    json["grid_steps"] = pointToArray(mapInfo->gridSteps());
    json["grid_zero"] = pointToArray(mapInfo->gridZero());
    json["map_generation"] = mapInfo->mapGeneration();
    json["map_max"] = pointToArray(mapInfo->mapMax());
    json["map_min"] = pointToArray(mapInfo->mapMin());

    return json;
}

QPointF MapinfoJsonConverter::arrayToQPoint(QJsonValue array)
{
    if (array.isArray())
    {
        QVariantList values = array.toArray().toVariantList();
        QList<float> floatValues;

        foreach (QVariant val, values)
        {
            floatValues.append(val.toInt(0));
        }

        if (floatValues.size() == 2)
        {
            return QPointF(floatValues.at(0), floatValues.at(1));
        }
    }


    return QPointF(0,0);

}

QJsonArray MapinfoJsonConverter::pointToArray(QPointF point)
{
    QJsonArray array;
    QPoint intPoint = point.toPoint();

    array.append(intPoint.x());
    array.append(intPoint.y());

    return array;
}
