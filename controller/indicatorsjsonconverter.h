// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef INDICATORSJSONCONVERTER_H
#define INDICATORSJSONCONVERTER_H

#include <QObject>
#include "qwtapi/model/indicators.h"

class IndicatorsJsonConverter : public QObject
{
    Q_OBJECT
public:
    explicit IndicatorsJsonConverter(QObject *parent = nullptr);
    Indicators* indicatorsFromJson(QString json);
    QJsonObject toJson(Indicators* indicators);

signals:

};

#endif // INDICATORSJSONCONVERTER_H
