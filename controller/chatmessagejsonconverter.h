// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef CHATMESSAGEJSONCONVERTER_H
#define CHATMESSAGEJSONCONVERTER_H

#include <QObject>
#include <QMap>
#include "qwtapi/model/chatmessages.h"
#include "qwtapi/model/chatmessage.h"

class ChatMessageJsonConverter : public QObject
{
    Q_OBJECT
public:
    explicit ChatMessageJsonConverter(QObject *parent = nullptr);
    ChatMessages* chatMessagesFromJson(QString json);
    QJsonArray toJson(ChatMessages* messages);

private:
    void addChatMessage(ChatMessages* messages, QJsonObject json);
    ChatMessage::Mode jsonValueToMode(QJsonValue jsonValue);


    QMap<QString, ChatMessage::Mode> m_modemap;
signals:

};

#endif // CHATMESSAGEJSONCONVERTER_H
