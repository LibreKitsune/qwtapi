// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPIMAGESERVICE_H
#define MAPIMAGESERVICE_H

#include <QObject>
#include <QPixmap>
#include <QImage>
#include "baseservice.h"

class MapImageService : public BaseService
{
    Q_OBJECT
public:
    explicit MapImageService(QObject* parent = nullptr);

public slots:
    void replyFinished(QNetworkReply *reply) override;

signals:
    void imageRecieved(QPixmap);
};

#endif // MAPIMAGESERVICE_H
