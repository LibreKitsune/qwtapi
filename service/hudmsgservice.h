// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef HUDMSGSERVICE_H
#define HUDMSGSERVICE_H

#include <QObject>
#include "baseservice.h"
#include "qwtapi/model/hudmsg.h"
#include "qwtapi/controller/hudmsgjsonconverter.h"


class HudMsgService : public BaseService
{
    Q_OBJECT
public:
    explicit HudMsgService(QObject *parent = nullptr);

private:
    HudMsgJsonConverter m_converter;

public slots:
    void replyFinished(QNetworkReply *reply) override;

signals:
    void hudMsgRecieved(HudMsg* hudMsg);

};

#endif // HUDMSGSERVICE_H
