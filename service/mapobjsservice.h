// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPOBJSSERVICE_H
#define MAPOBJSSERVICE_H

#include <QObject>
#include "baseservice.h"
#include "qwtapi/controller/mapobjsjsonconverter.h"
#include "qwtapi/model/mapobjs.h"
class MapObjsService : public BaseService
{
    Q_OBJECT
public:
    explicit MapObjsService(QObject* parent = nullptr);

private:
    MapObjsJsonConverter m_converter;

public slots:
    void replyFinished(QNetworkReply *reply) override;

signals:
    void mapObjsRecieved(MapObjs* objs);
};

#endif // MAPOBJSSERVICE_H
