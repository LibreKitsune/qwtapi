// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "missionservice.h"

MissionService::MissionService(QObject *parent)
 : BaseService{"http://127.0.0.1:8111/mission.json",parent}
{

}

void MissionService::replyFinished(QNetworkReply *reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        Mission* mission = m_converter.missionFromJson(QString::fromLocal8Bit(data));
        if (mission != nullptr)
        {
            emit missionRecieved(mission);
        }
    }
}
