// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef CHATMESSAGESSERVICE_H
#define CHATMESSAGESSERVICE_H

#include <QObject>
#include "baseservice.h"
#include "qwtapi/model/chatmessages.h"
#include "qwtapi/controller/chatmessagejsonconverter.h"

class ChatMessagesService : public BaseService
{
    Q_OBJECT
public:
    explicit ChatMessagesService(QObject *parent = nullptr);

private:
    ChatMessageJsonConverter m_converter;

public slots:
    void replyFinished(QNetworkReply *reply) override;

signals:
    void chatMessagesRecieved(ChatMessages* messages);
};

#endif // CHATMESSAGESSERVICE_H
