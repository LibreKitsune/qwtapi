// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapimageservice.h"

MapImageService::MapImageService(QObject* parent) :
    BaseService{"http://127.0.0.1:8111/map.img",parent}
{

}

void MapImageService::replyFinished(QNetworkReply* reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        QImage image = QImage::fromData(data);
        if (!image.isNull())
        {
            emit imageRecieved(QPixmap::fromImage(image));
        }
    }
}
