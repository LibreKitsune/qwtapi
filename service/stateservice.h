// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef STATESERVICE_H
#define STATESERVICE_H

#include <QObject>
#include "qwtapi/model/state.h"
#include "qwtapi/controller/statejsonconverter.h"
#include "baseservice.h"
#include <chrono>

class StateService : public BaseService
{
    Q_OBJECT
public:
    explicit StateService(QObject *parent = nullptr);

private:
    StateJsonConverter m_converter;

    int m_prevFuel;
    int m_fuel0;
    std::chrono::system_clock::time_point m_time0;
    double m_fuelConsumption;
    double m_fuelTimeRemaining;
    void calculateFuelConsumption(State* state);


public slots:
    void replyFinished(QNetworkReply *reply) override;

signals:
    void stateRecieved(State* state);
};

#endif // STATESERVICE_H
