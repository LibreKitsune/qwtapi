// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapobjsservice.h"

MapObjsService::MapObjsService(QObject* parent)
    : BaseService{"http://127.0.0.1:8111/map_obj.json",parent}
{

}

void MapObjsService::replyFinished(QNetworkReply* reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        MapObjs* mapObjs = m_converter.mapObjsFromJson(QString::fromLocal8Bit(data));
        if (mapObjs != nullptr)
        {
            emit mapObjsRecieved(mapObjs);
        }
    }
}
