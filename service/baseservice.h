// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef BASESERVICE_H
#define BASESERVICE_H

#include <QObject>
#include <QTimer>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

class BaseService : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int intervalms MEMBER m_intervalms READ intervalms WRITE setIntervalms NOTIFY intervalmsChanged)
public:
    explicit BaseService(QString url, QObject *parent = nullptr);
    int intervalms();
    void setIntervalms(int ms);

protected:
    QString m_url = "";
    int m_intervalms = 500;
    QTimer m_timer;
    QNetworkAccessManager *m_manager;
public slots:
    void start();
    void stop();
    void timerTick();
    virtual void replyFinished(QNetworkReply *reply) = 0;
signals:
    void intervalmsChanged(int ms);
};

#endif // BASESERVICE_H
