// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef INDICATORSSERVICE_H
#define INDICATORSSERVICE_H
#include <QObject>
#include "baseservice.h"
#include "qwtapi/controller/indicatorsjsonconverter.h"
#include "qwtapi/model/indicators.h"

class IndicatorsService : public BaseService
{
    Q_OBJECT
public:
    explicit IndicatorsService(QObject* parent = nullptr);

private:
    IndicatorsJsonConverter m_converter;

public slots:
    void replyFinished(QNetworkReply *reply) override;

signals:
    void indicatorsRecieved(Indicators* state);
};

#endif // INDICATORSSERVICE_H
