// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPINFOSERVICE_H
#define MAPINFOSERVICE_H

#include <QObject>
#include "baseservice.h"
#include "qwtapi/controller/mapinfojsonconverter.h"
#include "qwtapi/model/mapinfo.h"

class MapinfoService : public BaseService
{
    Q_OBJECT
public:
    explicit MapinfoService(QObject* parent = nullptr);

private:
    MapinfoJsonConverter m_converter;

public slots:
    void replyFinished(QNetworkReply *reply) override;

signals:
    void mapinfoRecieved(Mapinfo* info);
};

#endif // MAPINFOSERVICE_H
