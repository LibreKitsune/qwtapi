// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "chatmessagesservice.h"

ChatMessagesService::ChatMessagesService(QObject *parent)
    : BaseService("http://127.0.0.1:8111/gamechat?lastId=0",parent)
{

}

void ChatMessagesService::replyFinished(QNetworkReply *reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        ChatMessages* chatMessages = m_converter.chatMessagesFromJson(QString::fromLocal8Bit(data));
        if (chatMessages != nullptr)
        {
            emit chatMessagesRecieved(chatMessages);
        }
    }
}
