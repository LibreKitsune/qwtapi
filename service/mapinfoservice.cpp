// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapinfoservice.h"

MapinfoService::MapinfoService(QObject* parent)
    : BaseService{"http://127.0.0.1:8111/map_info.json",parent}
{

}

void MapinfoService::replyFinished(QNetworkReply *reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        Mapinfo* mapinfo = m_converter.mapinfoFromJson(QString::fromLocal8Bit(data));
        if (mapinfo != nullptr)
        {
            emit mapinfoRecieved(mapinfo);
        }
    }
}
