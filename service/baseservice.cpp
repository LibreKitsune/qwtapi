// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "baseservice.h"

BaseService::BaseService(QString url, QObject *parent)
    : QObject{parent}
{
    m_url = url;
    m_manager = new QNetworkAccessManager(this);
    m_manager->setAutoDeleteReplies(true);
    m_timer.setInterval(m_intervalms);
    m_timer.setSingleShot(false);
    connect(&m_timer, &QTimer::timeout, this, &BaseService::timerTick);
    connect(m_manager, &QNetworkAccessManager::finished, this, &BaseService::replyFinished);
}

int BaseService::intervalms()
{
    return m_intervalms;
}

void BaseService::setIntervalms(int ms)
{
    m_intervalms = ms;
    m_timer.setInterval(m_intervalms);
    emit intervalmsChanged(m_intervalms);
}

void BaseService::start()
{
    m_timer.start();
}

void BaseService::stop()
{
    m_timer.stop();
}

void BaseService::timerTick()
{
    m_manager->get(QNetworkRequest(QUrl(m_url)));
}
