// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "indicatorsservice.h"

IndicatorsService::IndicatorsService(QObject* parent)
    : BaseService{"http://127.0.0.1:8111/indicators",parent}
{

}

void IndicatorsService::replyFinished(QNetworkReply *reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        Indicators* indicators = m_converter.indicatorsFromJson(QString::fromLocal8Bit(data));
        if (indicators != nullptr)
        {
            emit indicatorsRecieved(indicators);
        }
    }
}
