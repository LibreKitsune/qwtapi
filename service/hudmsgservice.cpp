// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "hudmsgservice.h"

HudMsgService::HudMsgService(QObject *parent)
    : BaseService{"http://127.0.0.1:8111/hudmsg?lastEvt=0&lastDmg=0",parent}
{

}

void HudMsgService::replyFinished(QNetworkReply *reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        HudMsg* hudMsg = m_converter.hudMsgFromJson(QString::fromLocal8Bit(data));
        if (hudMsg != nullptr)
        {
            emit hudMsgRecieved(hudMsg);
        }
    }
}
