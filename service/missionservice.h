// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MISSIONSERVICE_H
#define MISSIONSERVICE_H

#include <QObject>
#include "qwtapi/model/mission.h"
#include "qwtapi/model/objective.h"
#include "qwtapi/controller/missionjsonconverter.h"
#include "baseservice.h"

class MissionService : public BaseService
{
    Q_OBJECT
public:
    explicit MissionService(QObject *parent = nullptr);

private:
    MissionJsonConverter m_converter;

public slots:
    void replyFinished(QNetworkReply* reply) override;

signals:
    void missionRecieved(Mission* mission);
};

#endif // MISSIONSERVICE_H
