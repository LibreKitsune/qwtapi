// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "stateservice.h"
#include <QtNetwork/QNetworkReply>

StateService::StateService(QObject *parent)
    : BaseService{"http://127.0.0.1:8111/state",parent}
{
    m_prevFuel = 0;
    m_fuel0 = 0;
    m_fuelConsumption = 0;
    m_fuelTimeRemaining = 0;
    m_time0 = std::chrono::system_clock::now();
}

void StateService::replyFinished(QNetworkReply *reply)
{
    if (reply->isFinished())
    {
        QByteArray data = reply->readAll();
        State* state = m_converter.stateFromJson(QString::fromLocal8Bit(data));
        if (state != nullptr)
        {
            calculateFuelConsumption(state);
            emit stateRecieved(state);
        }        
    }
}

void StateService::calculateFuelConsumption(State* state)
{
    double bufferTime = 2;
    if (state->valid())
    {
        std::chrono::system_clock::time_point currentTime = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedSecondsRaw0 = currentTime - m_time0;
        double elapsedSeconds0 = elapsedSecondsRaw0.count();

        if (m_prevFuel != state->currentFuel())
        {
            if (elapsedSeconds0 > bufferTime)
            {
                double fuelDelta = m_fuel0 - state->currentFuel();
                m_fuelConsumption = fuelDelta / elapsedSeconds0;
                m_fuelTimeRemaining = state->currentFuel() / m_fuelConsumption;
                m_time0 = std::chrono::system_clock::now();
                m_fuel0 = state->currentFuel();
            }

            m_prevFuel = state->currentFuel();

        }

        state->setFuelConsumption(m_fuelConsumption);
        state->setFuelTimeRemaining(m_fuelTimeRemaining);

    }
    else
    {
        m_fuelConsumption = 0;
        m_fuelTimeRemaining = 0;
        m_time0 = std::chrono::system_clock::now();
    }
}
