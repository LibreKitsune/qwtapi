// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "hudmsg.h"

HudMsg::HudMsg(QObject *parent)
    : QObject{parent}
{

}

QList<HudMsgDamage*> HudMsg::damageEvents()
{
    return m_damage;
}

void HudMsg::setDamageEvents(QList<HudMsgDamage*> damageEvents)
{
    m_damage = damageEvents;
    foreach (HudMsgDamage* dmg, damageEvents)
    {
        dmg->setParent(this);
    }
    emit damageChanged(m_damage);
}

QList<HudMsgEvent*> HudMsg::events()
{
    return m_event;
}

void HudMsg::setEvents(QList<HudMsgEvent*> events)
{
    m_event = events;
    foreach(HudMsgEvent* event, events)
    {
        event->setParent(this);
    }
    emit eventChanged(m_event);
}

void HudMsg::updateWith(HudMsg* msg)
{
    if (msg != nullptr)
    {
        setDamageEvents(msg->damageEvents());
        setEvents(msg->events());
    }
}
