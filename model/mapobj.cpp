// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapobj.h"

MapObj::MapObj(QObject *parent)
    : QObject{parent}
{

}

void MapObj::updateWith(MapObj* obj)
{
    setType(obj->type());
    setColour(obj->colour());
    setBlink(obj->blink());
    setIcon(obj->icon());
    setIconBG(obj->iconBG());
    setStart(obj->start());
    setEnd(obj->end());
    setDirection(obj->direction());
    setCoordinates(obj->coordinates());
}

MapObj::Type MapObj::type()
{
    return m_type;
}

void MapObj::setType(Type type)
{
    m_type = type;
    emit typeChanged(type);
}

QColor MapObj::colour()
{
    return m_colour;
}

void MapObj::setColour(QColor colour)
{
    m_colour = colour;
    emit colourChanged(colour);
}

bool MapObj::blink()
{
    return m_blink;
}

void MapObj::setBlink(bool blink)
{
    m_blink = blink;
    emit blinkChanged(blink);
}

MapObj::Icon MapObj::icon()
{
    return m_icon;
}

void MapObj::setIcon(Icon icon)
{
    m_icon = icon;
    emit iconChanged(icon);
}

MapObj::Icon MapObj::iconBG()
{
    return m_iconBG;
}

void MapObj::setIconBG(Icon icon)
{
    m_iconBG = icon;
    emit iconBGChanged(icon);
}

QPointF MapObj::start()
{
    return m_start;
}

void MapObj::setStart(QPointF start)
{
    m_start = start;
    emit startChanged(start);
}

QPointF MapObj::end()
{
    return m_end;
}

void MapObj::setEnd(QPointF end)
{
    m_end = end;
    emit endChanged(end);
}

QPointF MapObj::direction()
{
    return m_direction;
}

void MapObj::setDirection(QPointF direction)
{
    m_direction = direction;
    emit directionChanged(direction);
}

QPointF MapObj::coordinates()
{
    return m_coordinates;
}

void MapObj::setCoordinates(QPointF coordinates)
{
    m_coordinates = coordinates;
    emit coordinatesChanged(coordinates);
}
