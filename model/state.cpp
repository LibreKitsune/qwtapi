// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "state.h"

State::State(QObject *parent)
    : QObject{parent},
      m_valid(false),
      m_aileronPercent(0),
      m_elevatorPercent(0),
      m_rudderPercent(0),
      m_flapsPercent(0),
      m_gearPercent(0),
      m_airbrakePercent(0),
      m_altitude(0),
      m_trueAirSpeed(0),
      m_indicatedAirSpeed(0),
      m_mach(0),
      m_angleOfAttack(0),
      m_angleOfSideslip(0),
      m_gForce(0),
      m_climbRate(0),
      m_rollRate(0),
      m_currentFuel(0),
      m_maximumFuel(0),
      m_fuelConsumption(0),
      m_fuelTimeRemaining(0),
      m_engineCount(0)
{

}

bool State::valid()
{
    return m_valid;
}

void State::setValid(bool valid)
{
    if (m_valid != valid)
    {
        m_valid = valid;
        emit validChanged(valid);
    }
}

int State::aileronPercent()
{
    return m_aileronPercent;
}

void State::setAileronPercent(int percent)
{
    if (m_aileronPercent != percent)
    {
        m_aileronPercent = percent;
        emit aileronPercentChanged(percent);
    }
}

int State::elevatorPercent()
{
    return m_elevatorPercent;
}

void State::setElevatorPercent(int percent)
{
    if (m_elevatorPercent != percent)
    {
        m_elevatorPercent = percent;
        emit elevatorPercentChanged(percent);
    }
}

int State::rudderPercent()
{
    return m_rudderPercent;
}

void State::setRudderPercent(int percent)
{
    if (m_rudderPercent != percent)
    {
        m_rudderPercent = percent;
        emit rudderPercentChanged(percent);
    }
}

int State::flapsPercent()
{
    return m_flapsPercent;
}

void State::setFlapsPercent(int percent)
{
    if (m_flapsPercent != percent)
    {
        m_flapsPercent = percent;
        emit flapsPercentChanged(percent);
    }
}

int State::gearPercent()
{
    return m_gearPercent;
}

void State::setGearPercent(int percent)
{
    if (m_gearPercent != percent)
    {
        m_gearPercent = percent;
        emit gearPercentChanged(percent);
    }
}

int State::airbrakePercent()
{
    return m_airbrakePercent;
}

void State::setAirbrakePercent(int percent)
{
    if (m_airbrakePercent != percent)
    {
        m_airbrakePercent = percent;
        emit airbrakePercentChanged(percent);
    }
}

int State::altitude()
{
    return m_altitude;
}

void State::setAltitude(int altitude)
{
    if (m_altitude != altitude)
    {
        m_altitude = altitude;
        emit altitudeChanged(altitude);
    }
}

int State::trueAirSpeed()
{
    return m_trueAirSpeed;
}

void State::setTrueAirSpeed(int speed)
{
    if (m_trueAirSpeed != speed)
    {
        m_trueAirSpeed = speed;
        emit trueAirSpeedChanged(speed);
    }
}

int State::indicatedAirSpeed()
{
    return m_indicatedAirSpeed;
}

void State::setIndicatedAirSpeed(int speed)
{
    if (m_indicatedAirSpeed != speed)
    {
        m_indicatedAirSpeed = speed;
        emit indicatedAirSpeedChanged(speed);
    }
}

double State::mach()
{
    return m_mach;
}

void State::setMach(double mach)
{
    if (m_mach != mach)
    {
        m_mach = mach;
        emit machChanged(mach);
    }
}

double State::angleOfAttack()
{
    return m_angleOfAttack;
}

void State::setAngleOfAttack(double angle)
{
    if (m_angleOfAttack != angle)
    {
        m_angleOfAttack = angle;
        emit angleOfAttackChanged(angle);
    }
}

double State::angleOfSideslip()
{
    return m_angleOfSideslip;
}

void State::setAngleOfSideslip(double angle)
{
    if (m_angleOfSideslip != angle)
    {
        m_angleOfSideslip = angle;
        emit angleOfSideslipChanged(angle);
    }
}

double State::gForce()
{
    return m_gForce;
}

void State::setGForce(double force)
{
    if (m_gForce != force)
    {
        m_gForce = force;
        emit gForceChanged(force);
    }
}

double State::climbRate()
{
    return m_climbRate;
}

void State::setClimbRate(double rate)
{
    if (m_climbRate != rate)
    {
        m_climbRate = rate;
        emit climbRateChanged(rate);
    }
}

int State::rollRate()
{
    return m_rollRate;
}

void State::setRollRate(int rate)
{
    if (m_rollRate != rate)
    {
        m_rollRate = rate;
        emit rollRateChanged(rate);
    }
}

int State::currentFuel()
{
    return m_currentFuel;
}

void State::setCurrentFuel(int fuel)
{
    if (m_currentFuel != fuel)
    {
        m_currentFuel = fuel;
        emit currentFuelChanged(fuel);
    }
}

int State::maximumFuel()
{
    return m_maximumFuel;
}

void State::setMaximumFuel(int fuel)
{
    if (m_maximumFuel != fuel)
    {
        m_maximumFuel = fuel;
        emit maximumFuelChanged(fuel);
    }
}

double State::fuelConsumption()
{
    return m_fuelConsumption;
}

void State::setFuelConsumption(double consumption)
{
    if (m_fuelConsumption != consumption)
    {
        m_fuelConsumption = consumption;
        emit fuelConsumptionChanged(consumption);
    }
}

double State::fuelTimeRemaining()
{
    return m_fuelTimeRemaining;
}

void State::setFuelTimeRemaining(double remaining)
{
    if (m_fuelTimeRemaining != remaining)
    {
        m_fuelTimeRemaining = remaining;
        emit fuelTimeRemainingChanged(remaining);
    }
}


int State::engineCount()
{
    return m_engineCount;
}

void State::setEngineCount(int count)
{
    m_engineCount = count;
    emit engineCountChanged(count);
}

QList<EngineState*> State::engineStates()
{
    return m_engineStates;
}

void State::setEngineStates(QList<EngineState *> states)
{
    m_engineStates = states;
    foreach(EngineState* es, states)
    {
        es->setParent(this);
    }
    emit engineStatesChanged(states);
}

void State::addEngineState(EngineState *state)
{
    m_engineStates.append(state);
    state->setParent(this);
    emit engineStatesChanged(m_engineStates);
}

void State::updateWith(State* state)
{
    if (state != nullptr) {
        setValid(state->valid());
        setAileronPercent(state->aileronPercent());
        setElevatorPercent(state->elevatorPercent());
        setRudderPercent(state->rudderPercent());
        setFlapsPercent(state->flapsPercent());
        setGearPercent(state->gearPercent());
        setAirbrakePercent(state->airbrakePercent());
        setAltitude(state->altitude());
        setTrueAirSpeed(state->trueAirSpeed());
        setIndicatedAirSpeed(state->indicatedAirSpeed());
        setMach(state->mach());
        setAngleOfAttack(state->angleOfAttack());
        setAngleOfSideslip(state->angleOfSideslip());
        setGForce(state->gForce());
        setClimbRate(state->climbRate());
        setRollRate(state->rollRate());
        setCurrentFuel(state->currentFuel());
        setMaximumFuel(state->maximumFuel());
        setFuelConsumption(state->fuelConsumption());
        setFuelTimeRemaining(state->fuelTimeRemaining());
        setEngineCount(state->engineCount());
        setEngineStates(state->engineStates());
    }
}
