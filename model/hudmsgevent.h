// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef HUDMSGEVENT_H
#define HUDMSGEVENT_H

#include <QObject>
/*!
 * \brief The HudMsgEvent class
 */
class HudMsgEvent : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id MEMBER m_id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString msg MEMBER m_msg READ msg WRITE setMsg NOTIFY msgChanged)
    Q_PROPERTY(QString sender MEMBER m_sender READ sender WRITE setSender NOTIFY senderChanged)
    Q_PROPERTY(bool enemy MEMBER m_enemy READ enemy WRITE setEnemy NOTIFY enemyChanged)
    Q_PROPERTY(QString mode MEMBER m_mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(int time MEMBER m_time READ time WRITE setTime NOTIFY timeChanged)

public:
    explicit HudMsgEvent(QObject *parent = nullptr);

    /*!
     * \brief id
     * \return
     */
    int id();
    /*!
     * The msg of the event
     * \brief msg
     * \return
     */
    QString msg();
    /*!
     * \brief sender
     * \return
     */
    QString sender();
    /*!
     * \brief enemy
     * \return
     */
    bool enemy();
    /*!
     * \brief mode
     * \return
     */
    QString mode();
    /*!
     * \brief time
     * \return
     */
    int time();

    /*!
     * \brief setId
     * \param id
     */
    void setId(int id);
    /*!
     * \brief setMsg
     * \param msg
     */
    void setMsg(QString msg);
    /*!
     * \brief setSender
     * \param sender
     */
    void setSender(QString sender);
    /*!
     * \brief setEnemy
     * \param sender
     */
    void setEnemy(bool sender);
    /*!
     * \brief setMode
     * \param mode
     */
    void setMode(QString mode);
    /*!
     * \brief setTime
     * \param time
     */
    void setTime(int time);

private:
    int m_id;
    QString m_msg;
    QString m_sender;
    bool m_enemy;
    QString m_mode;
    int m_time;

signals:
    /*!
     * \brief idChanged
     * \param id
     */
    void idChanged(int id);
    /*!
     * \brief msgChanged
     * \param msg
     */
    void msgChanged(QString msg);
    /*!
     * \brief senderChanged
     * \param sender
     */
    void senderChanged(QString sender);
    /*!
     * \brief enemyChanged
     * \param enemy
     */
    void enemyChanged(bool enemy);
    /*!
     * \brief modeChanged
     * \param mode
     */
    void modeChanged(QString mode);
    /*!
     * \brief timeChanged
     * \param time
     */
    void timeChanged(int time);

public slots:
    void updateWith(HudMsgEvent* event);
};

#endif // HUDMSGEVENT_H
