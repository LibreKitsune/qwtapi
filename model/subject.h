// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef SUBJECT_H
#define SUBJECT_H

#include <QObject>

class Subject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name MEMBER m_name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString clan MEMBER m_clan READ clan WRITE setClan NOTIFY clanChanged)
    Q_PROPERTY(QString vehicle MEMBER m_vehicle READ vehicle WRITE setVehicle NOTIFY vehicleChanged)

public:
    explicit Subject(QObject *parent = nullptr);

    QString name();
    QString clan();
    QString vehicle();

    void setName(QString name);
    void setClan(QString clan);
    void setVehicle(QString vehicle);

private:
    QString m_name;
    QString m_clan;
    QString m_vehicle;

signals:
    void nameChanged(QString name);
    void clanChanged(QString clan);
    void vehicleChanged(QString vehicle);
public slots:
    void updateWith(Subject* subject);
};

#endif // SUBJECT_H
