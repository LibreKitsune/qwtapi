// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "objective.h"

Objective::Objective(QObject *parent)
    : QObject{parent}
{

}

void Objective::updateWith(Objective* objective)
{
    setPrimary(objective->primary());
    setStatus(objective->status());
    setText(objective->text());
}

bool Objective::primary()
{
    return m_primary;

}

void Objective::setPrimary(bool primary)
{
    m_primary = primary;
    emit primaryChanged(primary);
}

Objective::Status Objective::status()
{
    return m_status;
}

void Objective::setStatus(Objective::Status status)
{
    m_status = status;
    emit statusChanged(status);
}

QString Objective::text()
{
    return m_text;
}

void Objective::setText(QString text)
{
    m_text = text;
    emit textChanged(text);
}
