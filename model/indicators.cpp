// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "indicators.h"

Indicators::Indicators(QObject *parent)
    : QObject{parent}
{

}

bool Indicators::valid()
{
    return m_valid;
}

QString Indicators::type()
{
    return m_type;
}

QMap<QString, QVariant> Indicators::indicators()
{
    return m_indicators;
}

void Indicators::setValid(bool valid)
{
    m_valid = valid;
    emit validChanged(valid);
}

void Indicators::setType(QString type)
{
    m_type = type;
    emit typeChanged(type);
}

void Indicators::setIndicators(QMap<QString, QVariant> indicators)
{
    m_indicators = indicators;
    emit indicatorsChanged(indicators);
}

void Indicators::addIndicator(QString name, QVariant value)
{
    m_indicators.insert(name, value);
    emit indicatorsChanged(m_indicators);
}

void Indicators::updateWith(Indicators* indicators)
{
    setValid(indicators->valid());
    setType(indicators->type());
    setIndicators(indicators->indicators());
}
