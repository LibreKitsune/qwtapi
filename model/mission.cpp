// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mission.h"

Mission::Mission(QObject *parent)
    : QObject{parent}
{

}

void Mission::updateWith(Mission* mission)
{
    setStatus(mission->status());
    setObjectives(mission->objectives());
}

Objective::Status Mission::status()
{
    return m_status;
}

void Mission::setStatus(Objective::Status status)
{
    m_status = status;
    emit statusChanged(status);
}

QList<Objective*> Mission::objectives()
{
    return m_objectives;
}

void Mission::setObjectives(QList<Objective *> objectives)
{
    m_objectives = objectives;
    foreach(Objective *objective, m_objectives)
    {
        objective->setParent(this);
    }
    emit objectivesChanged(m_objectives);
}

void Mission::addObjective(Objective *objective)
{
    objective->setParent(this);
    m_objectives.append(objective);
    emit objectivesChanged(m_objectives);
}
