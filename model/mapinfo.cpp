// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapinfo.h"

Mapinfo::Mapinfo(QObject *parent)
    : QObject{parent}
{

}

void Mapinfo::updateWith(Mapinfo* mapInfo)
{
    setGridSize(mapInfo->gridSize());
    setGridSteps(mapInfo->gridSteps());
    setGridZero(mapInfo->gridZero());
    setMapGeneration(mapInfo->mapGeneration());
    setMapMax(mapInfo->mapMax());
    setMapMin(mapInfo->mapMin());
}

QPointF Mapinfo::gridSize()
{
    return m_gridSize;
}

void Mapinfo::setGridSize(QPointF size)
{
    m_gridSize = size;
    emit gridSizeChanged(size);
}

QPointF Mapinfo::gridSteps()
{
    return m_gridSteps;
}

void Mapinfo::setGridSteps(QPointF steps)
{
    m_gridSteps = steps;
    emit gridStepsChanged(steps);
}

QPointF Mapinfo::gridZero()
{
    return m_gridZero;
}

void Mapinfo::setGridZero(QPointF zero)
{
    m_gridZero = zero;
    emit gridZeroChanged(zero);
}

int Mapinfo::mapGeneration()
{
    return m_mapGenearation;
}

void Mapinfo::setMapGeneration(int generation)
{
    m_mapGenearation = generation;
    emit mapGenerationChanged(generation);
}

QPointF Mapinfo::mapMax()
{
    return m_mapMax;
}

void Mapinfo::setMapMax(QPointF max)
{
    m_mapMax = max;
    emit mapMaxChanged(max);
}

QPointF Mapinfo::mapMin()
{
    return m_mapMin;
}

void Mapinfo::setMapMin(QPointF min)
{
    m_mapMin = min;
    emit mapMinChanged(min);
}
