// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef INDICATORS_H
#define INDICATORS_H

#include <QObject>
#include <QMap>
#include <QVariant>
class Indicators : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool valid MEMBER m_valid READ valid WRITE setValid NOTIFY validChanged)
    Q_PROPERTY(QString type MEMBER m_type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QMap<QString, QVariant> indicators MEMBER m_indicators READ indicators WRITE setIndicators NOTIFY indicatorsChanged)
public:
    explicit Indicators(QObject *parent = nullptr);

    bool valid();
    QString type();
    QMap<QString, QVariant> indicators();

    void setValid(bool valid);
    void setType(QString type);
    void setIndicators(QMap<QString, QVariant> indicators);
    void addIndicator(QString name, QVariant value);

private:
    bool m_valid;
    QString m_type;
    QMap<QString, QVariant> m_indicators;

signals:
    void validChanged(bool valid);
    void typeChanged(QString type);
    void indicatorsChanged(QMap<QString, QVariant> indicators);

public slots:
    void updateWith(Indicators* indicators);

};

#endif // INDICATORS_H
