// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "chatmessage.h"

ChatMessage::ChatMessage(QObject *parent)
    : QObject{parent}
{

}

void ChatMessage::updateWith(ChatMessage* message)
{
    setId(message->id());
    setMsg(message->msg());
    setSender(message->sender());
    setEnemy(message->enemy());
    setMode(message->mode());
    setTime(message->time());
}

int ChatMessage::id()
{
    return m_id;
}

void ChatMessage::setId(int id)
{
    m_id = id;
    emit idChanged(id);
}

QString ChatMessage::msg()
{
    return m_msg;
}

void ChatMessage::setMsg(QString msg)
{
    m_msg = msg;
    emit msgChanged(msg);
}

QString ChatMessage::sender()
{
    return m_sender;
}

void ChatMessage::setSender(QString sender)
{
    m_sender = sender;
    emit senderChanged(sender);
}

bool ChatMessage::enemy()
{
    return m_enemy;
}

void ChatMessage::setEnemy(bool enemy)
{
    m_enemy = enemy;
    emit enemyChanged(enemy);
}

ChatMessage::Mode ChatMessage::mode()
{
    return m_mode;
}

void ChatMessage::setMode(ChatMessage::Mode mode)
{
    m_mode = mode;
    emit modeChanged(mode);
}

int ChatMessage::time()
{
    return m_time;
}

void ChatMessage::setTime(int time)
{
    m_time = time;
    emit timeChanged(time);
}
