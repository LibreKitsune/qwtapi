// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef OBJECTIVE_H
#define OBJECTIVE_H

#include <QObject>
class Objective : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool primary MEMBER m_primary READ primary WRITE setPrimary NOTIFY primaryChanged)
    Q_PROPERTY(Objective::Status status MEMBER m_status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(QString text MEMBER m_text READ text WRITE setText NOTIFY textChanged)

public:
    explicit Objective(QObject *parent = nullptr);

    enum class Status{ IN_PROGRESS, UNDEFINED, RUNNING, SUCCESS, FAIL, UNKNOWN };
    Q_ENUM(Status)

    bool primary();
    void setPrimary(bool primary);

    Objective::Status status();
    void setStatus(Objective::Status status);

    QString text();
    void setText(QString text);

private:
    bool m_primary;
    Objective::Status m_status;
    QString m_text;

signals:
    void primaryChanged(bool primary);
    void statusChanged(Objective::Status status);
    void textChanged(QString text);
public slots:
    void updateWith(Objective* objective);
};

#endif // OBJECTIVE_H
