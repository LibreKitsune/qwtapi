// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPOBJ_H
#define MAPOBJ_H

#include <QObject>
#include <QPointF>
#include <QColor>

class MapObj : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Type type MEMBER m_type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QColor colour MEMBER m_colour READ colour WRITE setColour NOTIFY colourChanged)
    Q_PROPERTY(bool blink MEMBER m_blink READ blink WRITE setBlink NOTIFY blinkChanged)
    Q_PROPERTY(Icon icon MEMBER m_icon READ icon WRITE setIcon NOTIFY iconChanged)
    Q_PROPERTY(Icon iconBG MEMBER m_iconBG READ iconBG WRITE setIconBG NOTIFY iconBGChanged)
    Q_PROPERTY(QPointF start MEMBER m_start READ start WRITE setStart NOTIFY startChanged)
    Q_PROPERTY(QPointF end MEMBER m_end READ end WRITE setEnd NOTIFY endChanged)
    Q_PROPERTY(QPointF direction MEMBER m_direction READ direction WRITE setDirection NOTIFY directionChanged)
    Q_PROPERTY(QPointF coordinates MEMBER m_coordinates READ coordinates WRITE setCoordinates NOTIFY coordinatesChanged)

public:
    explicit MapObj(QObject *parent = nullptr);

    enum class Type{ AIRCRAFT, AIRFIELD, BOMBING_POINT, DEFENDING_POINT, GROUND_MODEL, RESPAWN_BASE_BOMBER, UNKNOWN };
    enum class Icon{ AIRDEFENCE, FIGHTER, PLAYER, SPAA, TRACKED, WHEELED, BOMBING_POINT, DEFENDING_POINT, NONE, RESPAWN_BASE_BOMBER, UNKNOWN };

    Q_ENUM(Type)
    Q_ENUM(Icon)

    Type m_type;
    QColor m_colour;
    bool m_blink;
    Icon m_icon;
    Icon m_iconBG;
    QPointF m_start;
    QPointF m_end;
    QPointF m_direction;
    QPointF m_coordinates;

    Type type();
    void setType(Type type);

    QColor colour();
    void setColour(QColor colour);

    bool blink();
    void setBlink(bool blink);

    Icon icon();
    void setIcon(Icon icon);

    Icon iconBG();
    void setIconBG(Icon icon);

    QPointF start();
    void setStart(QPointF start);

    QPointF end();
    void setEnd(QPointF end);

    QPointF direction();
    void setDirection(QPointF direction);

    QPointF coordinates();
    void setCoordinates(QPointF coordinates);

signals:
    void typeChanged(MapObj::Type type);
    void colourChanged(QColor colour);
    void blinkChanged(bool blink);
    void iconChanged(MapObj::Icon icon);
    void iconBGChanged(MapObj::Icon icon);
    void startChanged(QPointF start);
    void endChanged(QPointF end);
    void directionChanged(QPointF direction);
    void coordinatesChanged(QPointF coordinates);

public slots:
    void updateWith(MapObj* obj);

};

#endif // MAPOBJ_H
