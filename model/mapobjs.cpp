// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mapobjs.h"

MapObjs::MapObjs(QObject *parent)
    : QObject{parent}
{

}

QList<MapObj*> MapObjs::objs()
{
    return m_objs;
}

void MapObjs::setObjs(QList<MapObj *> objs)
{
    m_objs = objs;
    foreach(MapObj* obj, objs)
    {
        obj->setParent(this);
    }
    emit objsChanged(objs);
}

void MapObjs::addObj(MapObj *obj)
{
    if (obj != nullptr)
    {
        obj->setParent(this);
        m_objs.append(obj);
    }
    emit objsChanged(m_objs);
}

void MapObjs::updateWith(MapObjs* objs)
{
    setObjs(objs->objs());
}
