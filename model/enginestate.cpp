// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "enginestate.h"

EngineState::EngineState(QObject *parent)
    : QObject{parent}
{

}

int EngineState::throttle()
{
    return m_throttle;
}

void EngineState::setThrottle(int throttle)
{
    m_throttle = throttle;
    emit throttleChanged(throttle);
}

int EngineState::rpmThrottle()
{
    return m_rpmThrottle;
}

void EngineState::setRpmThrottle(int throttle)
{
    m_rpmThrottle = throttle;
    emit rpmThrottleChanged(throttle);
}

int EngineState::radiator()
{
    return m_radiator;
}

void EngineState::setRadiator(int radiator)
{
    m_radiator = radiator;
    emit radiatorChanged(radiator);
}

int EngineState::magneto()
{
    return m_magneto;
}

void EngineState::setMagneto(int magneto)
{
    m_magneto = magneto;
    emit magnetoChanged(magneto);
}

double EngineState::power()
{
    return m_power;
}

void EngineState::setPower(double power)
{
    m_power = power;
    emit powerChanged(power);
}

int EngineState::rpm()
{
    return m_rpm;
}

void EngineState::setRpm(int rpm)
{
    m_rpm = rpm;
    emit rpmChanged(rpm);
}

double EngineState::manifoldPressure()
{
    return m_manifoldPressure;
}

void EngineState::setManifoldPressure(double pressure)
{
    m_manifoldPressure = pressure;
    emit manifoldPressureChanged(pressure);
}

int EngineState::waterTemp()
{
    return m_waterTemp;
}

void EngineState::setWaterTemp(int temp)
{
    m_waterTemp = temp;
    emit waterTempChanged(temp);
}

int EngineState::oilTemp()
{
    return m_oilTemp;
}

void EngineState::setOilTemp(int temp)
{
    m_oilTemp = temp;
    emit oilTempChanged(temp);
}

double EngineState::pitch()
{
    return m_pitch;
}

void EngineState::setPitch(double pitch)
{
    m_pitch = pitch;
    emit pitchChanged(pitch);
}

int EngineState::thrust()
{
    return m_thrust;
}

void EngineState::setThrust(int thrust)
{
    m_thrust = thrust;
    emit thrustChanged(thrust);
}

int EngineState::efficiency()
{
    return m_efficiency;
}

void EngineState::setEfficiency(int efficiency)
{
    m_efficiency = efficiency;
    emit efficiencyChanged(efficiency);
}

void EngineState::updateWith(EngineState* state)
{
    if (state != nullptr)
    {
        setThrottle(state->throttle());
        setRpmThrottle(state->rpmThrottle());
        setRadiator(state->radiator());
        setMagneto(state->magneto());
        setPower(state->power());
        setRpm(state->rpm());
        setManifoldPressure(state->manifoldPressure());
        setWaterTemp(state->waterTemp());
        setOilTemp(state->oilTemp());
        setPitch(state->pitch());
        setThrust(state->thrust());
        setEfficiency(state->efficiency());
    }
}
