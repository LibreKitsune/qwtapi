// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef ENGINESTATE_H
#define ENGINESTATE_H

#include <QObject>

class EngineState : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int throttle MEMBER m_throttle READ throttle WRITE setThrottle NOTIFY throttleChanged)
    Q_PROPERTY(int rpmThrottle MEMBER m_rpmThrottle READ rpmThrottle WRITE setRpmThrottle NOTIFY rpmThrottleChanged)
    Q_PROPERTY(int radiator MEMBER m_radiator READ radiator WRITE setRadiator NOTIFY radiatorChanged)
    Q_PROPERTY(int magneto MEMBER m_magneto READ magneto WRITE setMagneto NOTIFY magnetoChanged)
    Q_PROPERTY(double power MEMBER m_power READ power WRITE setPower NOTIFY powerChanged)
    Q_PROPERTY(int rpm MEMBER m_rpm READ rpm WRITE setRpm NOTIFY rpmChanged)
    Q_PROPERTY(double manifoldPressure MEMBER m_manifoldPressure READ manifoldPressure WRITE setManifoldPressure NOTIFY manifoldPressureChanged)
    Q_PROPERTY(int waterTemp MEMBER m_waterTemp READ waterTemp WRITE setWaterTemp NOTIFY waterTempChanged)
    Q_PROPERTY(int oilTemp MEMBER m_oilTemp READ oilTemp WRITE setOilTemp NOTIFY oilTempChanged)
    Q_PROPERTY(double pitch MEMBER m_pitch READ pitch WRITE setPitch NOTIFY pitchChanged)
    Q_PROPERTY(int thrust MEMBER m_thrust READ thrust WRITE setThrust NOTIFY thrustChanged)
    Q_PROPERTY(int efficiency MEMBER m_efficiency READ efficiency WRITE setEfficiency NOTIFY efficiencyChanged)
public:
    explicit EngineState(QObject *parent = nullptr);

    int throttle();
    int rpmThrottle();
    int radiator();
    int magneto();
    double power();
    int rpm();
    double manifoldPressure();
    int waterTemp();
    int oilTemp();
    double pitch();
    int thrust();
    int efficiency();

    void setThrottle(int throttle);
    void setRpmThrottle(int throttle);
    void setRadiator(int radiator);
    void setMagneto(int magneto);
    void setPower(double power);
    void setRpm(int rpm);
    void setManifoldPressure(double pressure);
    void setWaterTemp(int temp);
    void setOilTemp(int temp);
    void setPitch(double pitch);
    void setThrust(int thrust);
    void setEfficiency(int efficiency);

private:
    int m_throttle;
    int m_rpmThrottle;
    int m_radiator;
    int m_magneto;
    double m_power;
    int m_rpm;
    double m_manifoldPressure;
    int m_waterTemp;
    int m_oilTemp;
    double m_pitch;
    int m_thrust;
    int m_efficiency;

signals:
    void throttleChanged(int throttle);
    void rpmThrottleChanged(int throttle);
    void radiatorChanged(int radiator);
    void magnetoChanged(int magneto);
    void powerChanged(double power);
    void rpmChanged(int rpm);
    void manifoldPressureChanged(double pressure);
    void waterTempChanged(int temp);
    void oilTempChanged(int temp);
    void pitchChanged(double pitch);
    void thrustChanged(int thrust);
    void efficiencyChanged(int efficiency);
public slots:
    void updateWith(EngineState* state);
};

#endif // ENGINESTATE_H
