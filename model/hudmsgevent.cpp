// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "hudmsgevent.h"

HudMsgEvent::HudMsgEvent(QObject *parent)
    : QObject{parent}
{

}

int HudMsgEvent::id()
{
    return m_id;
}

QString HudMsgEvent::msg()
{
    return m_msg;
}

QString HudMsgEvent::sender()
{
    return m_sender;
}

bool HudMsgEvent::enemy()
{
    return m_enemy;
}

QString HudMsgEvent::mode()
{
    return m_mode;
}

int HudMsgEvent::time()
{
    return m_time;
}

void HudMsgEvent::setId(int id)
{
    m_id = id;
    emit idChanged(id);
}

void HudMsgEvent::setMsg(QString msg)
{
    m_msg = msg;
    emit msgChanged(msg);
}

void HudMsgEvent::setSender(QString sender)
{
    m_sender = sender;
    emit senderChanged(sender);
}

void HudMsgEvent::setEnemy(bool enemy)
{
    m_enemy = enemy;
    emit enemyChanged(enemy);
}

void HudMsgEvent::setMode(QString mode)
{
    m_mode = mode;
    emit modeChanged(mode);
}

void HudMsgEvent::setTime(int time)
{
    m_time = time;
    emit timeChanged(time);
}

void HudMsgEvent::updateWith(HudMsgEvent* event)
{
    if (event != nullptr)
    {
        setId(event->id());
        setMsg(event->msg());
        setSender(event->sender());
        setEnemy(event->enemy());
        setMode(event->mode());
        setTime(event->time());
    }
}
