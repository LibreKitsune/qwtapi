// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef CHATMESSAGE_H
#define CHATMESSAGE_H

#include <QObject>

class ChatMessage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id MEMBER m_id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString msg MEMBER m_msg READ msg WRITE setMsg NOTIFY msgChanged)
    Q_PROPERTY(QString sender MEMBER m_sender READ sender WRITE setSender NOTIFY senderChanged)
    Q_PROPERTY(bool enemy MEMBER m_enemy READ enemy WRITE setEnemy NOTIFY enemyChanged)
    Q_PROPERTY(ChatMessage::Mode mode MEMBER m_mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(int time MEMBER m_time READ time WRITE setTime NOTIFY timeChanged)

public:
    enum class Mode{ TEAM, SQUAD, ALL, SYSTEM, UNKNOWN };
    Q_ENUM(Mode)

    explicit ChatMessage(QObject *parent = nullptr);

    int id();
    void setId(int id);

    QString msg();
    void setMsg(QString msg);

    QString sender();
    void setSender(QString sender);

    bool enemy();
    void setEnemy(bool enemy);

    ChatMessage::Mode mode();
    void setMode(ChatMessage::Mode mode);

    int time();
    void setTime(int time);

private:
    int m_id;
    QString m_msg;
    QString m_sender;
    bool m_enemy;
    Mode m_mode;
    int m_time;

signals:
    void idChanged(int id);
    void msgChanged(QString msg);
    void senderChanged(QString sender);
    void enemyChanged(bool enemy);
    void modeChanged(ChatMessage::Mode mode);
    void timeChanged(int time);

public slots:
    void updateWith(ChatMessage* message);
};

#endif // CHATMESSAGE_H
