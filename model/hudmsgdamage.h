// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef HUDMSGDAMAGE_H
#define HUDMSGDAMAGE_H

#include <QObject>
#include "subject.h"

/*!
 * \brief The HudMsgDamage class
 */
class HudMsgDamage : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id MEMBER m_id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString msg MEMBER m_msg READ msg WRITE setMsg NOTIFY msgChanged)
    Q_PROPERTY(QString sender MEMBER m_sender READ sender WRITE setSender NOTIFY senderChanged)
    Q_PROPERTY(bool enemy MEMBER m_enemy READ enemy WRITE setEnemy NOTIFY enemyChanged)
    Q_PROPERTY(QString mode MEMBER m_mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(int time MEMBER m_time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(HudMsgDamage::Type type MEMBER m_type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(Subject* subject MEMBER m_subject READ subject WRITE setSubject NOTIFY subjectChanged)
    Q_PROPERTY(Subject* target MEMBER m_target READ target WRITE setTarget NOTIFY targetChanged)

public:
    explicit HudMsgDamage(QObject *parent = nullptr);

    enum Type{ SHOT_DOWN, DAMAGED, SET_AFIRE, ACHIEVED, DISCONNECTED, DESTROYED, CRASHED, FIRST_STRIKE, FINAL_BLOW, UNKNOWN };
    Q_ENUM(Type)

    /*!
     * \brief id
     * \return
     */
    int id();
    /*!
     * The msg of the event
     * \brief msg
     * \return
     */
    QString msg();
    /*!
     * \brief sender
     * \return
     */
    QString sender();
    /*!
     * \brief enemy
     * \return
     */
    bool enemy();
    /*!
     * \brief mode
     * \return
     */
    QString mode();
    /*!
     * \brief time
     * \return
     */
    int time();

    /*!
     * \brief type
     * \return
     */
    HudMsgDamage::Type type();

    /*!
     * \brief The player that triggered the event
     * \return
     */
    Subject* subject();

    /*!
     * \brief target name or achievement
     * \return
     */
    Subject* target();

    /*!
     * \brief setId
     * \param id
     */
    void setId(int id);
    /*!
     * \brief setMsg
     * \param msg
     */
    void setMsg(QString msg);
    /*!
     * \brief setSender
     * \param sender
     */
    void setSender(QString sender);
    /*!
     * \brief setEnemy
     * \param sender
     */
    void setEnemy(bool sender);
    /*!
     * \brief setMode
     * \param mode
     */
    void setMode(QString mode);
    /*!
     * \brief setTime
     * \param time
     */
    void setTime(int time);

    /*!
     * \brief setType
     * \param type
     */
    void setType(HudMsgDamage::Type type);

    /*!
     * \brief setSubject (takes ownership)
     * \param subject
     */
    void setSubject(Subject* subject);


    /*!
     * \brief setTarget (takes ownership)
     * \param target
     */
    void setTarget(Subject* target);

private:
    int m_id;
    QString m_msg;
    QString m_sender;
    bool m_enemy;
    QString m_mode;
    int m_time;
    HudMsgDamage::Type m_type;
    Subject* m_subject;
    Subject* m_target;

signals:
    /*!
     * \brief idChanged
     * \param id
     */
    void idChanged(int id);
    /*!
     * \brief msgChanged
     * \param msg
     */
    void msgChanged(QString msg);
    /*!
     * \brief senderChanged
     * \param sender
     */
    void senderChanged(QString sender);
    /*!
     * \brief enemyChanged
     * \param enemy
     */
    void enemyChanged(bool enemy);
    /*!
     * \brief modeChanged
     * \param mode
     */
    void modeChanged(QString mode);
    /*!
     * \brief timeChanged
     * \param time
     */
    void timeChanged(int time);

    /*!
     * \brief typeChanged
     * \param type
     */
    void typeChanged(HudMsgDamage::Type type);

    /*!
     * \brief subjectChanged
     * \param subject
     */
    void subjectChanged(Subject* subject);

    /*!
     * \brief targetChanged
     * \param target
     */
    void targetChanged(Subject* target);

public slots:
    void updateWith(HudMsgDamage* damage);
};

#endif // HUDMSGDAMAGE_H
