// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPINFO_H
#define MAPINFO_H

#include <QObject>
#include <QPointF>

class Mapinfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QPointF gridSize MEMBER m_gridSize READ gridSize WRITE setGridSize NOTIFY gridSizeChanged)
    Q_PROPERTY(QPointF gridSteps MEMBER m_gridSteps READ gridSteps WRITE setGridSteps NOTIFY gridStepsChanged)
    Q_PROPERTY(QPointF gridZero MEMBER m_gridZero READ gridZero WRITE setGridZero NOTIFY gridZeroChanged)
    Q_PROPERTY(int mapGeneration MEMBER m_mapGeneration READ mapGeneration WRITE setMapGeneration NOTIFY mapGenerationChanged)
    Q_PROPERTY(QPointF mapMax MEMBER m_mapMax READ mapMax WRITE setMapMax NOTIFY mapMaxChanged)
    Q_PROPERTY(QPointF mapMin MEMBER m_mapMin READ mapMin WRITE setMapMin NOTIFY mapMinChanged)

private:
    QPointF m_gridSize;
    QPointF m_gridSteps;
    QPointF m_gridZero;
    int m_mapGenearation;
    QPointF m_mapMax;
    QPointF m_mapMin;

public:
    explicit Mapinfo(QObject *parent = nullptr);

    QPointF gridSize();
    void setGridSize(QPointF size);

    QPointF gridSteps();
    void setGridSteps(QPointF steps);

    QPointF gridZero();
    void setGridZero(QPointF zero);

    int mapGeneration();
    void setMapGeneration(int generation);

    QPointF mapMax();
    void setMapMax(QPointF max);

    QPointF mapMin();
    void setMapMin(QPointF min);

signals:
    void gridSizeChanged(QPointF size);
    void gridStepsChanged(QPointF steps);
    void gridZeroChanged(QPointF zero);
    void mapGenerationChanged(int generation);
    void mapMaxChanged(QPointF max);
    void mapMinChanged(QPointF min);

public slots:
    void updateWith(Mapinfo* mapInfo);


};

#endif // MAPINFO_H
