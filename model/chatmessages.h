// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef CHATMESSAGES_H
#define CHATMESSAGES_H

#include <QObject>
#include "chatmessage.h"
class ChatMessages : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<ChatMessage*> messages MEMBER m_messages READ messages WRITE setMessages NOTIFY messagesChanged)
public:
    explicit ChatMessages(QObject *parent = nullptr);
    QList<ChatMessage*> messages();
    void setMessages(QList<ChatMessage*> messages);
    void addMessage(ChatMessage* message);

private:
    QList<ChatMessage*> m_messages;

signals:
    void messagesChanged(QList<ChatMessage*> messages);

public slots:
    void updateWith(ChatMessages* messages);
};

#endif // CHATMESSAGES_H
