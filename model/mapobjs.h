// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAPOBJS_H
#define MAPOBJS_H

#include <QObject>
#include "mapobj.h"

class MapObjs : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<MapObj*> objs MEMBER m_objs READ objs WRITE setObjs NOTIFY objsChanged)

public:
    explicit MapObjs(QObject *parent = nullptr);
    QList<MapObj*> objs();
    void setObjs(QList<MapObj*> objs);
    void addObj(MapObj* obj);

private:
    QList<MapObj*> m_objs;

public slots:
    void updateWith(MapObjs* objs);

signals:
    void objsChanged(QList<MapObj*> objs);
};

#endif // MAPOBJS_H
