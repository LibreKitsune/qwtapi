// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "hudmsgdamage.h"

HudMsgDamage::HudMsgDamage(QObject *parent)
    : QObject{parent}
{

}

int HudMsgDamage::id()
{
    return m_id;
}

QString HudMsgDamage::msg()
{
    return m_msg;
}

QString HudMsgDamage::sender()
{
    return m_sender;
}

bool HudMsgDamage::enemy()
{
    return m_enemy;
}

QString HudMsgDamage::mode()
{
    return m_mode;
}

int HudMsgDamage::time()
{
    return m_time;
}

HudMsgDamage::Type HudMsgDamage::type()
{
    return m_type;
}

Subject* HudMsgDamage::subject()
{
    return m_subject;
}

Subject* HudMsgDamage::target()
{
    return m_target;
}

void HudMsgDamage::setId(int id)
{
    m_id = id;
    emit idChanged(id);
}

void HudMsgDamage::setMsg(QString msg)
{
    m_msg = msg;
    emit msgChanged(msg);
}

void HudMsgDamage::setSender(QString sender)
{
    m_sender = sender;
    emit senderChanged(sender);
}

void HudMsgDamage::setEnemy(bool enemy)
{
    m_enemy = enemy;
    emit enemyChanged(enemy);
}

void HudMsgDamage::setMode(QString mode)
{
    m_mode = mode;
    emit modeChanged(mode);
}

void HudMsgDamage::setTime(int time)
{
    m_time = time;
    emit timeChanged(time);
}

void HudMsgDamage::setType(HudMsgDamage::Type type)
{
    m_type = type;
    emit typeChanged(type);
}

void HudMsgDamage::setSubject(Subject* subject)
{
    m_subject = subject;
    subject->setParent(this);
    emit subjectChanged(subject);
}

void HudMsgDamage::setTarget(Subject* target)
{
    m_target = target;
    target->setParent(this);
    emit targetChanged(target);
}

void HudMsgDamage::updateWith(HudMsgDamage* damage)
{
    if (damage != nullptr)
    {
        setId(damage->id());
        setMsg(damage->msg());
        setSender(damage->sender());
        setEnemy(damage->enemy());
        setMode(damage->mode());
        setTime(damage->time());
        setType(damage->type());
        setSubject(damage->subject());
        setTarget(damage->target());
    }
}
