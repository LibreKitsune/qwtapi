// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef STATE_H
#define STATE_H

#include <QObject>
#include "enginestate.h"

class State : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool valid MEMBER m_valid READ valid WRITE setValid NOTIFY validChanged)
    Q_PROPERTY(int aileronPercent MEMBER m_aileronPercent READ aileronPercent WRITE setAileronPercent NOTIFY aileronPercentChanged)
    Q_PROPERTY(int elevatorPercent MEMBER m_elevatorPercent READ elevatorPercent WRITE setElevatorPercent NOTIFY elevatorPercentChanged)
    Q_PROPERTY(int rudderPercent MEMBER m_rudderPercent READ rudderPercent WRITE setRudderPercent NOTIFY rudderPercentChanged)
    Q_PROPERTY(int flapsPercent MEMBER m_flapsPercent READ flapsPercent WRITE setFlapsPercent NOTIFY flapsPercentChanged)
    Q_PROPERTY(int gearPercent MEMBER m_gearPercent READ gearPercent WRITE setGearPercent NOTIFY gearPercentChanged)
    Q_PROPERTY(int airbrakePercent MEMBER m_airbrakePercent READ airbrakePercent WRITE setAirbrakePercent NOTIFY airbrakePercentChanged)
    Q_PROPERTY(int altitude MEMBER m_altitude READ altitude WRITE setAltitude NOTIFY altitudeChanged)
    Q_PROPERTY(int trueAirSpeed MEMBER m_trueAirSpeed READ trueAirSpeed WRITE setTrueAirSpeed NOTIFY trueAirSpeedChanged)
    Q_PROPERTY(int indicatedAirSpeed MEMBER m_indicatedAirSpeed READ indicatedAirSpeed WRITE setIndicatedAirSpeed NOTIFY indicatedAirSpeedChanged)
    Q_PROPERTY(double mach MEMBER m_mach READ mach WRITE setMach NOTIFY machChanged)
    Q_PROPERTY(double angleOfAttack MEMBER m_angleOfAttack READ angleOfAttack WRITE setAngleOfAttack NOTIFY angleOfAttackChanged)
    Q_PROPERTY(double angleOfSideslip MEMBER m_angleOfSideslip READ angleOfSideslip WRITE setAngleOfSideslip NOTIFY angleOfSideslipChanged)
    Q_PROPERTY(double gForce MEMBER m_gForce READ gForce WRITE setGForce NOTIFY gForceChanged)
    Q_PROPERTY(double climbRate MEMBER m_climbRate READ climbRate WRITE setClimbRate NOTIFY climbRateChanged)
    Q_PROPERTY(int rollRate MEMBER m_rollRate READ rollRate WRITE setRollRate NOTIFY rollRateChanged)
    Q_PROPERTY(int currentFuel MEMBER m_currentFuel READ currentFuel WRITE setCurrentFuel NOTIFY currentFuelChanged)
    Q_PROPERTY(int maximumFuel MEMBER m_maximumFuel READ maximumFuel WRITE setMaximumFuel NOTIFY maximumFuelChanged)
    Q_PROPERTY(double fuelConsumption MEMBER m_fuelConsumption READ fuelConsumption WRITE setFuelConsumption NOTIFY fuelConsumptionChanged)
    Q_PROPERTY(double fuelTimeRemaining MEMBER m_fuelTimeRemaining READ fuelTimeRemaining WRITE setFuelTimeRemaining NOTIFY fuelTimeRemainingChanged)
    Q_PROPERTY(int engineCount MEMBER m_engineCount READ engineCount WRITE setEngineCount NOTIFY engineCountChanged)
    Q_PROPERTY(QList<EngineState*> engineStates MEMBER m_engineStates READ engineStates WRITE setEngineStates NOTIFY engineStatesChanged)

public:
    explicit State(QObject *parent = nullptr);

    bool valid();
    int aileronPercent();
    int elevatorPercent();
    int rudderPercent();
    int flapsPercent();
    int gearPercent();
    int airbrakePercent();
    int altitude();
    int trueAirSpeed();
    int indicatedAirSpeed();
    double mach();
    double angleOfAttack();
    double angleOfSideslip();
    double gForce();
    double climbRate();
    int rollRate();
    int currentFuel();
    double fuelConsumption();
    double fuelTimeRemaining();
    int maximumFuel();
    int engineCount();
    QList<EngineState*> engineStates();

    void setValid(bool valid);
    void setAileronPercent(int percent);
    void setElevatorPercent(int percent);
    void setRudderPercent(int percent);
    void setFlapsPercent(int percent);
    void setGearPercent(int percent);
    void setAirbrakePercent(int percent);
    void setAltitude(int altitude);
    void setTrueAirSpeed(int speed);
    void setIndicatedAirSpeed(int speed);
    void setMach(double mach);
    void setAngleOfAttack(double angle);
    void setAngleOfSideslip(double angle);
    void setGForce(double force);
    void setClimbRate(double rate);
    void setRollRate(int rate);
    void setCurrentFuel(int fuel);
    void setMaximumFuel(int fuel);
    void setFuelConsumption(double consumption);
    void setFuelTimeRemaining(double remaining);
    void setEngineCount(int count);
    void setEngineStates(QList<EngineState*> states);
    void addEngineState(EngineState* state);



private:
    bool m_valid;
    int m_aileronPercent;
    int m_elevatorPercent;
    int m_rudderPercent;
    int m_flapsPercent;
    int m_gearPercent;
    int m_airbrakePercent;
    // H, m
    int m_altitude;
    int m_trueAirSpeed;
    int m_indicatedAirSpeed;
    double m_mach;
    double m_angleOfAttack;
    double m_angleOfSideslip;
    // Ny
    double m_gForce;
    // Vy
    double m_climbRate;
    // wx
    int m_rollRate;
    // Mfuel
    int m_currentFuel;
    // Mfuel0
    int m_maximumFuel;
    double m_fuelConsumption;
    double m_fuelTimeRemaining;
    int m_engineCount;
    QList<EngineState*> m_engineStates;

signals:
    void validChanged(bool valid);
    void aileronPercentChanged(int percent);
    void elevatorPercentChanged(int percent);
    void rudderPercentChanged(int percent);
    void flapsPercentChanged(int percent);
    void gearPercentChanged(int percent);
    void airbrakePercentChanged(int percent);
    void altitudeChanged(int altitude);
    void trueAirSpeedChanged(int speed);
    void indicatedAirSpeedChanged(int speed);
    void machChanged(double mach);
    void angleOfAttackChanged(double angle);
    void angleOfSideslipChanged(double angle);
    void gForceChanged(double force);
    void climbRateChanged(double rate);
    void rollRateChanged(int rate);
    void currentFuelChanged(int fuel);
    void maximumFuelChanged(int fuel);
    void fuelConsumptionChanged(double fuelConsumption);
    void fuelTimeRemainingChanged(double timeRemaining);
    void engineCountChanged(int count);
    void engineStatesChanged(QList<EngineState*> states);
public slots:
    void updateWith(State* state);
};

#endif // STATE_H
