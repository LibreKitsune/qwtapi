// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MISSION_H
#define MISSION_H

#include <QObject>
#include "objective.h"

class Mission : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Objective::Status status MEMBER m_status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(QList<Objective*> objectives MEMBER m_objectives READ objectives WRITE setObjectives NOTIFY objectivesChanged)

public:
    explicit Mission(QObject *parent = nullptr);

    Objective::Status status();
    void setStatus(Objective::Status status);

    QList<Objective*> objectives();
    void setObjectives(QList<Objective*> objectives);
    void addObjective(Objective* objective);

private:
    Objective::Status m_status;
    QList<Objective*> m_objectives;

signals:
    void statusChanged(Objective::Status status);
    void objectivesChanged(QList<Objective*> objectives);

public slots:
    void updateWith(Mission* mission);
};

#endif // MISSION_H
