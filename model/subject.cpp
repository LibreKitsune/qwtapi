// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "subject.h"

Subject::Subject(QObject *parent)
    : QObject{parent}
{
    m_name = "";
    m_clan = "";
    m_vehicle = "";
}

QString Subject::name()
{
    return m_name;
}

QString Subject::clan()
{
    return m_clan;
}

QString Subject::vehicle()
{
    return m_vehicle;
}

void Subject::setName(QString name)
{
    m_name = name;
    emit nameChanged(name);
}

void Subject::setClan(QString clan)
{
    m_clan = clan;
    emit clanChanged(clan);
}

void Subject::setVehicle(QString vehicle)
{
    m_vehicle = vehicle;
    emit vehicleChanged(vehicle);
}

void Subject::updateWith(Subject* subject)
{
    if (subject != nullptr)
    {
        setName(subject->name());
        setClan(subject->clan());
        setVehicle(subject->vehicle());
    }
}
