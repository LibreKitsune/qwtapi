// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "chatmessages.h"

ChatMessages::ChatMessages(QObject *parent)
    : QObject{parent}
{

}

void ChatMessages::updateWith(ChatMessages* messages)
{
    setMessages(messages->messages());
}

QList<ChatMessage*> ChatMessages::messages()
{
    return m_messages;
}

void ChatMessages::setMessages(QList<ChatMessage *> messages)
{
    m_messages = messages;
    foreach(ChatMessage *message, messages)
    {
        message->setParent(this);
    }
    emit messagesChanged(messages);
}

void ChatMessages::addMessage(ChatMessage *message)
{
    m_messages.append(message);
    message->setParent(this);
    emit messagesChanged(m_messages);
}
