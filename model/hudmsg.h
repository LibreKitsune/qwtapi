// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef HUDMSG_H
#define HUDMSG_H

#include <QObject>
#include <QList>
#include "hudmsgdamage.h"
#include "hudmsgevent.h"

class HudMsg : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QList<HudMsgDamage*> damage MEMBER m_damage READ damageEvents WRITE setDamageEvents NOTIFY damageChanged)
    Q_PROPERTY(QList<HudMsgEvent*> event MEMBER m_event READ events WRITE setEvents NOTIFY eventChanged)

private:
    QList<HudMsgDamage*> m_damage;
    QList<HudMsgEvent*> m_event;

public:
    explicit HudMsg(QObject *parent = nullptr);

    QList<HudMsgDamage*> damageEvents();
    void setDamageEvents(QList<HudMsgDamage*> damageEvents) ;
    QList<HudMsgEvent*> events() ;
    void setEvents(QList<HudMsgEvent*> events);

signals:
    void damageChanged(const QList<HudMsgDamage*> &newdamage);
    void eventChanged(const QList<HudMsgEvent*> &newevent);

public slots:
    void updateWith(HudMsg* msg);

};

#endif // HUDMSG_H
