<!--
SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>

SPDX-License-Identifier: EUPL-1.2
-->

<!--[![Please Don't ship WIP](https://img.shields.io/badge/Please-Don't%20Ship%20WIP-yellow)](https://dont-ship.it/)-->

# qWTapi

Qt library for the WarThunder localhost API

## Description
A Qt library to help developing tools that interact with Warthunders localhost API.

An example implementation of this library can be found [here](https://gitlab.com/LibreKitsune/qwtapi-example)

## License
EUPL-1.2

## Project status
Currently experimental and under heavy developement. But should be in a usable state.

### Progress
- [x] HudMsg (Killfeed)
    - [x] Parsing
    - [x] API Access
- [x] State (Aircraft Core stats)
    - [x] Parsing
    - [x] API Access
- [x] gamechat (Gamechat)
    - [x] Parsing
    - [x] API Access
- [x] mission (Mission Objectives)
    - [x] Parsing
    - [x] API Access
- [x] indicators (Aircraft specific indicators)
    - [x] Parsing
    - [x] API Access
- [x] map_info (Map Size and Grid steps)
    - [x] Parsing
    - [x] API Access
- [x] map_obj (Map entities with x,y)
    - [x] Parsing
    - [x] API Access
- [x] map_img (The map as an image)
    - [x] Parsing
    - [x] API Access
